/***************************************************************************
                          CPUCyclesCounter.cpp  -  description
                             -------------------
    begin                : 2015/02/05
    copyright            : (C) 2015 by Tomá¨ Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "CPUCyclesCounter.h"

CPUCyclesCounter defaultCPUCyclesCounter;

CPUCyclesCounter::CPUCyclesCounter()
{
   reset();
}

void CPUCyclesCounter::reset()
{
   stop_cycles = rdtsc();
   total_cycles = 0;
   stop_state = false;
}

void CPUCyclesCounter::stop()
{
   if( ! stop_state )
   {
      total_cycles += rdtsc() - stop_cycles;
      stop_state = true;
  }
}

void CPUCyclesCounter::start()
{
   stop_cycles = rdtsc();
   stop_state = false;
}

unsigned long long int CPUCyclesCounter::getCycles()
{
   stop();
   start();
   return total_cycles;
}



