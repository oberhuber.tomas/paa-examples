/***************************************************************************
                          CPUCyclesCounter.h  -  description
                             -------------------
    begin                : 2015/02/05
    copyright            : (C) 2015 by Tomáš Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CPUCyclesCounterH
#define CPUCyclesCounterH

class CPUCyclesCounter
{
   public:

   CPUCyclesCounter();

   void reset();

   void stop();

   void start();

   unsigned long long getCycles();

   protected:

   static inline unsigned long long rdtsc();

   unsigned long long stop_cycles;

   unsigned long long total_cycles;

   bool stop_state;
};

extern CPUCyclesCounter defaultCpuCyclesCounter;

inline unsigned long long CPUCyclesCounter::rdtsc()
{
  unsigned hi, lo;
  __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
  return ( ( unsigned long long ) lo ) | ( ( ( unsigned long long ) hi ) << 32 );
}

#endif
