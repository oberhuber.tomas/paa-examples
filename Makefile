include Makefile.inc

HEADERS = \
          PapiCounter.h \

SOURCES = \
          PapiCounter.cpp

OBJECTS = \
          PapiCounter.o
MAKEFILES = \
            Makefile \
            Makefile.inc

CXX_FLAGS += -I.

include profiling/Makefile
include sequential-architectures/Makefile
include vectorization/Makefile
include shared-memory-architectures/Makefile
include mpi/Makefile
include omp/Makefile
include cuda/Makefile

DIST = $(SOURCES) $(HEADERS) $(MAKEFILES)

all: $(TARGETS)

install: all
	cp $(TARGETS) $(INSTALL_DIR)/bin

dist: $(DIST)
	tar zcvf para-src.tgz $(DIST)

clean:
	rm -f $(OBJECTS)

%.o: %.cpp
	$(CXX) -c -o $@ $(CXX_FLAGS) $<

%.o: %.cu
	$(CUDA_CXX) -c -o $@ $(CUDA_CXX_FLAGS) $<
