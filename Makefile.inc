INSTALL_DIR = ${HOME}/.local
TNL_DIR = ${HOME}/.local

CXX = g++
#CXX_FLAGS = -DNDEBUG -O3 -funroll-loops -g -std=c++17 -Dlinux -I ${TNL_DIR}/include
#CXX_FLAGS = -funroll-loops -g -std=c++17 -Dlinux -I ${TNL_DIR}/include
CXX_FLAGS = -DNDEBUG -O3 -g -std=c++17 -Dlinux -ftree-vectorizer-verbose=0  -mavx2  -fopenmp -DHAVE_OMP -I ${TNL_DIR}/include
# -ftree-vectorize -march=native
MPI_CXX = mpic++

# Uncomment the following line to enable CUDA
WITH_CUDA = yes
CUDA_CXX = nvcc

# Uncomment the following to enable MPI
#CXX = mpic++
#CXX_FLAGS = -DNDEBUG -O3 -DHAVE_MPI -std=c++17

# PAPI setup
#CXX_FLAGS += -DHAVE_PAPI
LD_FLAGS += -lpapi -lgomp

# libNUMA setup
CXX_FLAGS += -DHAVE_NUMA
LD_FLAGS += -lnuma


LD_FLAGS += -lgomp
CUDA_CXX = nvcc
CUDA_CXX_FLAGS = -DNDEBUG -O3 -DHAVE_CUDA -arch sm_50
CUDA_LD_FLAGS = $(LD_FLAGS) -L/usr/local/cuda/lib64 -lcudart
