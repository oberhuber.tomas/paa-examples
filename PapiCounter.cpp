/***************************************************************************
                          PapiCounter.cpp  -  description
                             -------------------
    begin                : 2015/02/05
    copyright            : (C) 2015 by Tomá¨ Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <cstring>
#include <PapiCounter.h>

using std::cerr;
using std::endl;


PapiCounter::PapiCounter()
: numberOfEvents( 0 ), lastEvent( 0 ), events( 0 ), eventsId( 0 ), eventsAux( 0 )
{
}

bool PapiCounter::setNumberOfEvents( int numberOfEvents )
{
#ifdef HAVE_PAPI
   int counters = PAPI_num_counters();
   bool returnCode( true );
   this->numberOfEvents = numberOfEvents;
   if( numberOfEvents > counters )
   {
      std::cerr << "I cannot set " << numberOfEvents << " PAPI events." << std::endl;
      std::cerr << "There are only " << counters << " hardware counters available on this system." << std::endl;      
      this->numberOfEvents = counters;
      returnCode = false;
   }
   this->events = new long long int[ this->numberOfEvents ];
   this->eventsId = new int[ this->numberOfEvents ];
   this->eventsAux = new long long int[ this->numberOfEvents ];
   this->lastEvent = 0;
   this->reset();
   return returnCode;
#endif
   return false;
}

bool PapiCounter::addEvent( int event )
{
   if( this->lastEvent < this->numberOfEvents )
   {
      this->eventsId[ lastEvent++ ] = event;
      return true;
   }
   else
   {
      std::cerr << "I am not able to add another event." << std::endl;
      return false;
   }
}

void PapiCounter::reset()
{
   memset( this->events, 0, this->numberOfEvents * sizeof( long long int ) );
   memset( this->eventsAux, 0, this->numberOfEvents * sizeof( long long int ) );
}

void PapiCounter::start()
{
#ifdef HAVE_PAPI
   PAPI_start_counters( this->eventsId, this->numberOfEvents );
#endif
}

void PapiCounter::stop()
{
#ifdef HAVE_PAPI
   PAPI_stop_counters( this->eventsAux, this->numberOfEvents );
   for( int i = 0; i < this->numberOfEvents; i++ )
      this->events[ i ] += this->eventsAux[ i ];
#endif
}

int PapiCounter::getEventValue( int event )
{
   for( int i = 0; i < this->numberOfEvents; i++ )
   {
      if( this->eventsId[ i ] == event )
         return this->events[ i ];
   }
   return -1;
}

PapiCounter::~PapiCounter()
{
   delete[] this->events;
   delete[] this->eventsId;
   delete[] this->eventsAux;
}

