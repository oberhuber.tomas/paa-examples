/***************************************************************************
                          PapiCounter.h  -  description
                             -------------------
    begin                : 2015/02/05
    copyright            : (C) 2015 by Tomá¨ Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PapiCounterH
#define PapiCounterH

#ifdef HAVE_PAPI
#include <papi.h>
#endif

class PapiCounter
{
   public:

   PapiCounter();

   bool setNumberOfEvents( int events );

   bool addEvent( int event );

   void reset();

   void stop();

   void start();

   int getEventValue( int event );

   ~PapiCounter();

   protected:

   int numberOfEvents, lastEvent;

   int* eventsId;

   long long int* events;

   long long int* eventsAux;
};

#endif
