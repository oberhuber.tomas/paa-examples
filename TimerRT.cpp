#include "TimerRT.h"

//#ifdef HAVE_SYS_TIME_H
   #include <stddef.h>
   #include <sys/time.h>
   #define HAVE_TIME
//#endif


TimerRT defaultTimer;

TimerRT :: TimerRT()
{
   reset();
}

void TimerRT :: reset()
{
#ifdef HAVE_TIME
	struct timeval tp;
	int rtn = gettimeofday( &tp, NULL );
	stop_time = ( double ) tp. tv_sec + 1.0e-6 * ( double ) tp. tv_usec;
	total_time = 0.0;
	stop_state = false;
#else
	stop_time = 0.0;
#endif

}

void TimerRT :: stop()
{
#ifdef HAVE_TIME
	if( ! stop_state )
	{
		struct timeval tp;
		int rtn = gettimeofday( &tp, NULL );
		total_time += ( double ) tp. tv_sec + 1.0e-6 * ( double ) tp. tv_usec - stop_time;
		stop_state = true;
	}
#endif
}

void TimerRT :: start()
{
#ifdef HAVE_TIME
	struct timeval tp;
	int rtn = gettimeofday( &tp, NULL );
	stop_time = ( double ) tp. tv_sec + 1.0e-6 * ( double ) tp. tv_usec;
	stop_state = false;
#endif
}

double TimerRT :: getTime()
{
#ifdef HAVE_TIME
	stop();
	start();
	return total_time;
#endif
	return -1;
}




