#ifndef TimerRTH
#define TimerRTH

class TimerRT
{
   public:

   TimerRT();

   void reset();

   void stop();

   void start();

   double getTime();

   protected:

   double stop_time;

   double total_time;

   bool stop_state;
};

extern TimerRT defaultTimer;
#endif



