// Implemented by Jakub Klinkovský for his dissertation
// https://dspace.cvut.cz/handle/10467/112277

__global__ void kernel_axpy(index_t size, value_t alpha, value_t* x, value_t* y)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < size)
        y[i] = alpha * x[i] + y[i];

}

__global__ void kernel_axpy(index_t size, value_t alpha, value_t* x, value_t* y)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < size)
        y[i] = alpha * x[i] + y[i];

}

void parallel_axpy(index_t size, value_t alpha, value_t* x, value_t* y)
{
    cudaError_t err;
    // allocate the x and y arrays in device memory
    value_t* device_x, device_y;
    err = cudaMalloc((void**) &device_x, sizeof(value_t) * size);
    checkErr(err, "cudaMalloc");
    err = cudaMalloc((void**) &device_y, sizeof(value_t) * size);
    checkErr(err, "cudaMalloc");
    // copy the x and y arrays from host memory to device memory
    err = cudaMemcpy(device_x, x, sizeof(value_t) * size, cudaMemcpyHostToDevice);
    checkErr(err, "cudaMemcpy");
    err = cudaMemcpy(device_y, y, sizeof(value_t) * size, cudaMemcpyHostToDevice);
    checkErr(err, "cudaMemcpy");
    dim3 blockSize, gridSize;
    blockSize.x = 256;
    gridSize.x = (size + blockSize.x - 1) / blockSize.x;
    kernel_axpy<<<gridSize, blockSize>>>(size, alpha, device_x, device_y);

   // wait for the kernel to finish before reading back results
    cudaDeviceSynchronize();

    // copy the y array from device memory to host memory
    err = cudaMemcpy(y, device_y, sizeof(value_t) * size, cudaMemcpyDeviceToHost);
    checkErr(err, "cudaMemcpy");

    // release allocated resources
    cudaFree(device_x);
    cudaFree(device_y);
}