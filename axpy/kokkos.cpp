// Implemented by Jakub Klinkovský for his dissertation
// https://dspace.cvut.cz/handle/10467/112277

int main(int argc, char** argv)
{
    Kokkos::initialize(argc, argv);

    // create host views for the input arrays
    using host_view = Kokkos::View<value_t*, Kokkos::HostSpace>;
    host_view x_host(x_raw, size);
    host_view y_host(y_raw, size);

    // define the execution space and accessible view type
    using exe_t = Kokkos::DefaultExecutionSpace;
    using view_t = Kokkos::View<value_t*, typename exe_t::memory_space>;

   // mirror the views in the memory space of exe_t
    view_t x = Kokkos::create_mirror_view_and_copy(exe_t(), x_host);
    view_t y = Kokkos::create_mirror_view_and_copy(exe_t(), y_host);

    // execute the kernel on the device
    using RangePolicy = Kokkos::RangePolicy<exe_t>;
    Kokkos::parallel_for(
        "axpy",                         // name of the kernel
        RangePolicy(0, size),           // iteration range
        KOKKOS_LAMBDA (index_t i) {     // kernel function
            y[i] = alpha * x[i] + y[i];
        }
    );

    // copy the y array from device memory to host memory
    Kokkos::deep_copy(y_host, y);

    Kokkos::finalize();
    return EXIT_SUCCESS;
}

