// Implemented by Jakub Klinkovský for his dissertation
// https://dspace.cvut.cz/handle/10467/112277

struct kernel_axpy;

void parallel_axpy(index_t size, value_t alpha, value_t* x, value_t* y)
{
    // create a queue for a default-selected device
    auto queue = sycl::queue{sycl::default_selector{}};

    // allocate the x and y arrays in device memory
    value_t* device_x = sycl::malloc_device<value_t>(size, queue);
    value_t* device_y = sycl::malloc_device<value_t>(size, queue);
    if (!device_x || !device_y) {
        std::cerr << "ERROR: could not allocate device arrays" << std::endl;
        std::terminate();
    }

    // copy the x and y arrays from host memory to device memory
    queue.memcpy(device_x, x, sizeof(value_t) * size).wait();
    queue.memcpy(device_y, y, sizeof(value_t) * size).wait();

    // execute a device kernel
    queue.parallel_for<kernel_axpy>(
        sycl::range(size),
        [=](sycl::item<1> item) {
            auto i = item.get_id();
            device_y[i] = alpha * device_x[i] + device_y[i];
        }
    ).wait();

    // copy the y array from device memory to host memory
    queue.memcpy(y, device_y, sizeof(value_t) * size).wait();

    // release allocated resources
    sycl::free(device_x, queue);
    sycl::free(device_y, queue);
}
