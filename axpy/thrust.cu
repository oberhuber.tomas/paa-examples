// Implemented by Jakub Klinkovský for his dissertation
// https://dspace.cvut.cz/handle/10467/112277

void parallel_axpy_host(index_t size, value_t alpha, value_t* x, value_t* y)
{
    thrust::transform(
        thrust::host,                            // execution backend
        x,                                       // beginning of the first input sequence
        x + size,                                // end of the first input sequence
        y,                                       // beginning of the second input sequence
        y,                                       // beginning of the output sequence
        [alpha](value_t x_val, value_t y_val)    // transformation function applied to
        { return alpha * x_val + y_val; }        // pairs of input elements
    );
}

void parallel_axpy_device(index_t size, value_t alpha, value_t* x, value_t* y)
{
    // allocate arrays on the device
    thrust::device_vector<value_t> x_device(size);
    thrust::device_vector<value_t> y_device(size);

    // copy the arrays from host to device
    thrust::copy(x, x + size, x_device.begin());
    thrust::copy(y, y + size, y_device.begin());

    // perform the axpy operation
    thrust::transform(
        thrust::device,                          // execution backend
        x_device.begin(),                        // beginning of the first input sequence
        x_device.end(),                          // end of the first input sequence
        y_device.begin(),                        // beginning of the second input sequence
        y_device.begin(),                        // beginning of the output sequence
        [alpha] __device__ (value_t x_val, value_t y_val) // transformation function applied to
        { return alpha * x_val + y_val; }                 // pairs of input elements
    );

    // copy the result from device to host
    thrust::copy(y_device.begin(), y_device.end(), y);
}
