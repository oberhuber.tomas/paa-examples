#include <cuda.h>
#include <cuda_runtime_api.h>
#include<iostream>

__global__ void cudaVectorAddition( double* cuda_u,
                                    const double* cuda_v,
                                    const int size,
                                    const int gridIdx,
                                    const int gridDim );

int main( int argc, char* argv[] )
{
    const int size( 1 << 20 );    
    double *host_u, *host_v, *cuda_u, *cuda_v;

    /****
     * Allocation on the host
     */
    host_u = new double[ size ];
    host_v = new double[ size ];

    /****
     * Allocation on the device
     */
    if( cudaMalloc( ( void** ) & cuda_u, size * sizeof( double ) ) != cudaSuccess ||
        cudaMalloc( ( void** ) & cuda_v, size * sizeof( double ) ) != cudaSuccess )
    {
        std::cerr << "Unable to allocate vectors on the device." << std::endl;
        return EXIT_FAILURE;
    }

    /****
     * Setting-up the vectors
     */
    for( int i = 0; i < size; i++ )
    {
        host_u[ i ] = i;
        host_v[ i ] = size - i;
    }
    if( cudaMemcpy( ( void* ) cuda_u, ( void* ) host_u, size * sizeof( double ), cudaMemcpyHostToDevice ) != cudaSuccess ||
        cudaMemcpy( ( void* ) cuda_v, ( void* ) host_v, size * sizeof( double ), cudaMemcpyHostToDevice ) != cudaSuccess )
    {
        std::cerr << "Unable to copy data from the host to the device." << std::endl;
        return EXIT_FAILURE;
    }

    /****
     * Compute the addition on the CPU
     */
    for( int i = 0; i < size; i ++ )
        host_u[ i ] += host_v[ i ];

    /****
     * Run the CUDA kernel
     */
    dim3 cudaBlockSize( 256 );
    cudaDeviceProp properties;
    cudaGetDeviceProperties( &properties, 0 );
    int maxCudaGridSize( properties.maxGridSize[ 0 ] );
    const int cudaBlocksCount = size / cudaBlockSize.x + ( size % cudaBlockSize.x != 0 );
    const int cudaGridsCount = cudaBlocksCount / maxCudaGridSize + ( cudaBlocksCount % maxCudaGridSize != 0 );
    for( int gridIdx = 0; gridIdx < cudaGridsCount; gridIdx ++ )
    {
        dim3 cudaGridSize;
        if( gridIdx < cudaGridsCount )
            cudaGridSize.x = maxCudaGridSize;
        else
            cudaGridSize.x = cudaBlocksCount % maxCudaGridSize;
        cudaVectorAddition<<< cudaGridSize, cudaBlockSize >>>( cuda_u, cuda_v, size, gridIdx, maxCudaGridSize );
        cudaError err = cudaGetLastError();
        if( err != cudaSuccess )
        {
            std::cerr << "Computation on the device failed with error: " << cudaGetErrorString( err ) << "." << std::endl;
            return EXIT_FAILURE;
        }
    }

    /****
     * Copy the result back to the host
     */
    if( cudaMemcpy( ( void* ) host_v, ( void* ) cuda_u, size * sizeof( double ), cudaMemcpyDeviceToHost ) != cudaSuccess )
    {
        std::cerr << "Unabel to copy data back from the GPU." << std::endl;
        return EXIT_FAILURE;
    }

    if( memcmp( ( void* ) host_u, ( void* ) host_v, size * sizeof( double ) ) != 0 )
    {
        std::cerr << "The results are different." << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << "Everything is ok." << std::endl;

    /****
     * Freeing allocated memory
     */
    delete[] host_u;
    delete[] host_v;
    cudaFree( cuda_u );
    cudaFree( cuda_v );
}

__global__ void cudaVectorAddition( double* cuda_u,
                                    const double* cuda_v,
                                    const int size,
                                    const int gridIdx,
                                    const int gridDim )
{
    const int tid = ( gridIdx * gridDim + blockIdx.x ) * blockDim.x + threadIdx.x;
    if( tid < size )
        cuda_u[ tid ] += cuda_v[ tid ];
}
