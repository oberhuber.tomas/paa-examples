#include <iostream>
#include <mpi.h>
#include <iomanip>
#include "../TimerRT.h"

int main(int argc, char *argv[])
{
    int rank, size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    const int max_N = 1 << 24;
    const int elements_to_send = max_N * 4;
    int *send_data = new int[max_N];
    int *recv_data = new int[max_N];

    TimerRT timer;
    for( int comm_size = 2; comm_size < size; comm_size *= 2 )
    {
        if( rank == 0 )
            std::cout << "===================================================" << std::endl;

        MPI_Comm new_communicator;
        MPI_Comm_split(MPI_COMM_WORLD, rank < comm_size ? 1 : MPI_UNDEFINED, rank, &new_communicator);
        if( rank < comm_size ){
            int new_size, new_rank;
            for (int N = 1; N <= max_N; N *= 2)
            {
                timer.reset();
                timer.start();
                for( int sent_elements = 0; sent_elements < elements_to_send; sent_elements += N )
                    for( int target = 1; target < comm_size; target++) {
                        if (rank == 0)
                            MPI_Send(send_data, N, MPI_INT, target, 0, new_communicator);
                        else if (rank == target)
                            MPI_Recv(recv_data, N, MPI_INT, 0, 0, new_communicator, MPI_STATUS_IGNORE);
                    }
                MPI_Barrier(new_communicator);
                timer.stop();
                if( rank == 0 ){
                    const double bw = (double) elements_to_send * sizeof( int ) / timer.getTime() / 1.0e9;
                    std::cout << "Send-Receive: Comm. size = " << comm_size << " N = " << std::setw( 8 ) << N << " BW = " << bw << " GB/sec." << std::endl;
                }

                timer.reset();
                timer.start();
                for( int sent_elements = 0; sent_elements < elements_to_send; sent_elements += N )
                MPI_Bcast(send_data, N, MPI_INT, 0, new_communicator);
                MPI_Barrier(new_communicator);
                timer.stop();
                if( rank == 0 ){
                    const double bw = (double) elements_to_send * sizeof( int ) / timer.getTime() / 1.0e9;
                    std::cout << "Broadcast:    Comm. size = " << comm_size << " N = " << std::setw( 8 ) << N << " BW = " << bw << " GB/sec." << std::endl;
                }
            }
        }
    }
    delete[] send_data;
    delete[] recv_data;
    MPI_Finalize();
    return EXIT_SUCCESS;
}
