#include <iostream>
#include <mpi.h>

int main(int argc, char *argv[])
{
    int rank, size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int max_N = 1 << 12;
    int *send_data = new int[max_N];
    int *recv_data = new int[max_N];

    for (int N = 1; N <= max_N; N *= 2)
    {
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0)
        {
            MPI_Send(send_data, N, MPI_INT, 1, 0, MPI_COMM_WORLD);
            std::cout << "Process 0 sent message to process 1:       N = " << N << std::endl;
            MPI_Recv(recv_data, N, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            std::cout << "Process 0 received message from process 1: N = " << N << std::endl;
        }
        else if (rank == 1)
        {
            MPI_Send(send_data, N, MPI_INT, 0, 0, MPI_COMM_WORLD);
            std::cout << "Process 1 sent message to process 0:       N = " << N << std::endl;
            MPI_Recv(recv_data, N, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            std::cout << "Process 1 received message from process 0: N = " << N << std::endl;
        }
    }
    delete[] send_data;
    delete[] recv_data;
    MPI_Finalize();
    return 0;
}
