#include <iostream>
#include <mpi.h>

int main(int argc, char *argv[])
{
    int rank, size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Request send_request, recv_request;

    int max_N = 1 << 12;
    int *send_data = new int[max_N];
    int *recv_data = new int[max_N];

    for (int N = 1; N <= max_N; N *= 2)
    {
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0)
        {
            MPI_Isend(send_data, N, MPI_INT, 1, 0, MPI_COMM_WORLD, &send_request);
            std::cout << "Process 0 sent message to process 1:       N = " << N << std::endl;
            MPI_Irecv(recv_data, N, MPI_INT, 1, 0, MPI_COMM_WORLD, &recv_request);
            std::cout << "Process 0 received message from process 1: N = " << N << std::endl;
        }
        else if (rank == 1)
        {
            MPI_Isend(send_data, N, MPI_INT, 0, 0, MPI_COMM_WORLD, &send_request);
            std::cout << "Process 1 sent message to process 0:       N = " << N << std::endl;
            MPI_Irecv(recv_data, N, MPI_INT, 0, 0, MPI_COMM_WORLD, &recv_request);
            std::cout << "Process 1 received message from process 0: N = " << N << std::endl;
        }
    }

    // Perform some computations
    int a(0);
    for (int i = 0; i < 100000; ++i)
    {
        a += i / 1000;
    }

    int flag;
    MPI_Test(&send_request, &flag, MPI_STATUS_IGNORE);
    if (flag)
    {
        std::cout << "Process " << rank << " already received the message." << std::endl;
    }
    else
        MPI_Wait(&send_request, MPI_STATUS_IGNORE);

    MPI_Wait(&recv_request, MPI_STATUS_IGNORE);
    delete[] send_data;
    delete[] recv_data;
    MPI_Finalize();
    return 0;
}
