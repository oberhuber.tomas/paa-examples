#include <iostream>
#include <mpi.h>

int main(int argc, char *argv[])
{
    int rank, size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int message = rank;
    if (rank == 0)
    {
        MPI_Status status;
        for (int i = 1; i < size; i++)
        {
            MPI_Recv(&message, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
            std::cout << "Process 0 received message " << message << " from process " << status.MPI_SOURCE << std::endl;
        }
    }
    else
    {
        MPI_Send(&message, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }
    MPI_Finalize();
    return 0;
}
