#include <iostream>
#include <mpi.h>

int main(int argc, char *argv[])
{
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int data = rank;
    MPI_Bcast(&data, 1, MPI_INT, 0, MPI_COMM_WORLD);
    std::cout << "Process " << rank << " received broadcasted value: " << data << std::endl;

    int reduced_value;
    data = rank;
    MPI_Reduce(&data, &reduced_value, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0)
    {
        std::cout << "Process 0: Sum of squares from all processes: " << reduced_value << std::endl;
    }
    MPI_Finalize();
    return 0;
}
