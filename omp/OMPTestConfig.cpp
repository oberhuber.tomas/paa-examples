/***************************************************************************
                          OMPTestConfig.cpp  -  description
                             -------------------
    begin                : 2017/03/07
    copyright            : (C) 2017 by Tomá¹ Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cstring>
#include <cstdlib>
#include <iostream>
#include "OMPTestConfig.h"

using std::cerr;
using std::cout;
using std::endl;

OMPTestConfig config;

OMPTestConfig::OMPTestConfig()
    : outputFile("memory-access-test.txt"),
      testType("sequential"),
      initialVectorSize(128),
      maxVectorSize(1 << 28),
      elementsPerTest(1 << 24),
      numThreads(1),
      verbose(true)
{
}

bool OMPTestConfig::parseCommandLineArguments(int argc, char *argv[])
{
   int i(0);
   for (int i = 1; i < argc; i++)
   {
      if (strcmp(argv[i], "--output-file") == 0 ||
          strcmp(argv[i], "-o") == 0)
      {
         this->outputFile = argv[++i];
         continue;
      }
      if (strcmp(argv[i], "--test-type") == 0)
      {
         this->testType = argv[++i];
         continue;
      }
      if (strcmp(argv[i], "--initial-vector-size") == 0)
      {
         this->initialVectorSize = atoi(argv[++i]);
         continue;
      }
      if (strcmp(argv[i], "--max-vector-size") == 0)
      {
         this->maxVectorSize = atoi(argv[++i]);
         continue;
      }
      if (strcmp(argv[i], "--elements-per-test") == 0)
      {
         this->elementsPerTest = atoi(argv[++i]);
         continue;
      }
      if (strcmp(argv[i], "--threads") == 0)
      {
         this->numThreads = atoi(argv[++i]);
         continue;
      }

      if (strcmp(argv[i], "--no-verbose") == 0)
      {
         this->verbose = false;
         continue;
      }

      if (strcmp(argv[i], "--help") == 0)
      {
         printHelp(argv[0]);
         return false;
      }
      cerr << "Unknown command line argument " << argv[i] << ". Use --help for more information." << endl;
      return false;
   }
   return true;
}

void OMPTestConfig::printHelp(const char *programName)
{
   cout << "Use of " << programName << ":" << endl
        << endl
        << "  --output-file or -o       Name of the output file with the test result. Can be processed with Gnuplot." << endl
        << "  --test-type               Test type can be static or dynamic." << endl
        << "  --initial-vector-size     Size in bytes of the test vector in the first loop of the test." << endl
        << "  --max-vector-size         Maximum size in bytes of the test vector." << endl
        << "  --elements-per-test       If the test array has n elements then the test loop will be repeated ceil((elements-per-test)/n) times." << endl
        << "  --threads                 Number of threads. It is 1 by default." << endl
        << "  --no-verbose              Turns off the verbose mode." << endl;
}
