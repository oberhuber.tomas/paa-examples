/***************************************************************************
                          OMPTestConfig.h  -  description
                             -------------------
    begin                : 2017/03/07
    copyright            : (C) 2017 by Tomá¨ Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#pragma once

class OMPTestConfig
{
   public:

      OMPTestConfig();

      bool parseCommandLineArguments( int argc, char* argv[] );

      void printHelp( const char* programName );

      const char* outputFile;

      const char* testType;

      int initialVectorSize,
          maxVectorSize,
          elementsPerTest,
          numThreads;

      bool verbose;
};

