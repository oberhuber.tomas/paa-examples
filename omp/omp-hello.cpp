#include <omp.h>
#include <stdlib.h>
#include <iostream>

int main(int argc, char **argv)
{
#pragma omp parallel
   {
      std::cout << "Hello from thread number "
                << omp_get_thread_num() << " of "
                << omp_get_num_threads() << " threads."
                << std::endl;
   }

   std::cout << std::endl
             << "Now with critical section:" << std::endl;
#pragma omp parallel
   {
#pragma omp critical
      std::cout << "Hello from thread number "
                << omp_get_thread_num() << " of "
                << omp_get_num_threads() << " threads."
                << std::endl;
   }

   std::cout << std::endl
             << "Now with single section:" << std::endl;
#pragma omp parallel
   {
#pragma omp single
      std::cout << "Hello from thread number "
                << omp_get_thread_num() << " of "
                << omp_get_num_threads() << " threads."
                << std::endl;
   }

   std::cout << std::endl
             << "Now with master section:" << std::endl;
#pragma omp parallel
   {
#pragma omp master
      std::cout << "Hello from thread number "
                << omp_get_thread_num() << " of "
                << omp_get_num_threads() << " threads."
                << std::endl;
   }

   std::cout << std::endl
             << "Now two greetings:" << std::endl;
#pragma omp parallel
   {
#pragma omp critical(print)
      std::cout << "Hello number ONE from thread number "
                << omp_get_thread_num() << " of "
                << omp_get_num_threads() << " threads."
                << std::endl;

#pragma omp critical(print)
      std::cout << "Hello number TWO from thread number "
                << omp_get_thread_num() << " of "
                << omp_get_num_threads() << " threads."
                << std::endl;
   }

   std::cout << std::endl
             << "... and now two greetings with barrier:" << std::endl;
#pragma omp parallel
   {
#pragma omp critical(print)
      std::cout << "Hello number ONE from thread number "
                << omp_get_thread_num() << " of "
                << omp_get_num_threads() << " threads."
                << std::endl;
#pragma omp barrier
#pragma omp critical(print)
      std::cout << "Hello number TWO from thread number "
                << omp_get_thread_num() << " of "
                << omp_get_num_threads() << " threads."
                << std::endl;
   }
   return EXIT_SUCCESS;
}
