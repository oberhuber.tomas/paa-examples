#include <omp.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <cstring>
#include <limits>
#include <CPUCyclesCounter.h>
#include <TimerRT.h>
#include <PapiCounter.h>

#include "OMPTestConfig.h"

// #pragma omp declare reduction( maxVal: dbl_int: omp_out=max( omp_out, omp_in ) )

int maxSequential(const int *v, const int size)
{
   int res(std::numeric_limits<int>::lowest());
   for (int i = 0; i < size; i++)
      res = std::max(res, v[i]);
   return res;
}

int maxOmpWrong(const int *v, const int size)
{
   int res(std::numeric_limits<int>::lowest());
#pragma omp parallel for shared(res), schedule(static), firstprivate(v)
   for (int i = 0; i < size; i++)
   {
      res = std::max(res, v[i]);
   }
   return res;
}

int maxOmpAtomic(const int *v, const int size)
{
   int res(std::numeric_limits<int>::lowest());
#pragma omp parallel for shared(res), schedule(static), firstprivate(v)
   for (int i = 0; i < size; i++)
   {
      const int aux = v[i];
#pragma omp atomic compare
      if (res < aux)
      {
         res = aux;
      }
   }
   return res;
}

int maxOmpAtomicOpt(const int *v, const int size)
{
   int res(std::numeric_limits<int>::lowest());
#pragma omp parallel for shared(res), schedule(static), firstprivate(v)
   for (int i = 0; i < size; i++)
   {
      const int aux = v[i];
      if (res < aux)
      {
#pragma omp atomic compare
         if (res < aux)
         {
            res = aux;
         }
      }
   }
   return res;
}

int maxOmpFalseSharing(const int *v, const int size)
{
   int res(std::numeric_limits<int>::lowest());
   const int threads_count = omp_get_max_threads();
   int *aux = new int[threads_count];
   for (int i = 0; i < threads_count; i++)
      aux[i] = res;
#pragma omp parallel shared(res), firstprivate(v)
   {
      const int thread_id = omp_get_thread_num();
#pragma omp for schedule(static)
      for (int i = 0; i < size; i++)
         aux[thread_id] = std::max(aux[thread_id], v[i]);
   }
   for (int i = 0; i < threads_count; i++)
      res = std::max(res, aux[i]);
   return res;
}

int maxOmpNoFalseSharing(const int *v, const int size)
{
   int res(std::numeric_limits<int>::lowest());
   const int cache_line = 64 / sizeof(int);
   const int threads_count = omp_get_max_threads();
   int *aux = new int[threads_count * cache_line];
   for (int i = 0; i < threads_count; i++)
      aux[i * cache_line] = res;
#pragma omp parallel shared(res), firstprivate(v)
   {
      const int idx = omp_get_thread_num() * cache_line;
#pragma omp for schedule(static)
      for (int i = 0; i < size; i++)
         aux[idx] = std::max(aux[idx], v[i]);
   }
   for (int i = 0; i < threads_count; i++)
      res = std::max(res, aux[i * cache_line]);
   return res;
}

int maxOmpReduction(const int *v, const int size)
{
#pragma omp declare reduction(maxVal:int : omp_out = omp_in < omp_out ? omp_out : omp_in), initializer(omp_priv = std::numeric_limits<int>::lowest())
   int res;
#pragma omp parallel for reduction(maxVal : res), schedule(static), firstprivate(v)
   for (int i = 0; i < size; i++)
   {
      res = std::max(res, v[i]);
   }
   return res;
}

void writeTableHeader(std::ostream &str)
{
   str << std::right;
   str << "#";
   str << std::setw(200) << std::setfill('-') << "-" << std::endl;
   str << "#";
   str << std::setfill(' ');
   str << std::setw(20) << "Vector size"
       << std::setw(20) << "log2 vector size"
       << std::setw(50) << "CPU tics               " << std::endl;
   str << "#";
   str << std::setw(40) << ""
       << std::setw(20) << "Sequential"
       << std::setw(20) << "OMP wrong"
       << std::setw(20) << "OMP atomic"
       << std::setw(20) << "OMP atomic opt."
       << std::setw(25) << "OMP false sharing"
       << std::setw(25) << "OMP no false sharing"
       << std::setw(20) << "OMP reduction" << std::endl;
   str << "#";
   str << std::setw(220) << std::setfill('-') << "-" << std::endl;
}
void writeTableLine(std::ostream &str,
                    const unsigned long long int size,
                    const unsigned long long int testedElements,
                    const unsigned long long int sequentialCpuCycles,
                    const unsigned long long int ompWrongCpuCycles,
                    const unsigned long long int ompAtomicCpuCycles,
                    const unsigned long long int ompAtomicOptCpuCycles,
                    const unsigned long long int ompFalseSharingCpuCycles,
                    const unsigned long long int ompNoFalseSharingCpuCycles,
                    const unsigned long long int ompReductionCpuCycles)
{
   str << std::setw(20) << size
       << std::setw(20) << log2(size)
       << std::setw(20) << sequentialCpuCycles / (double)testedElements
       << std::setw(20) << ompWrongCpuCycles / (double)testedElements
       << std::setw(20) << ompAtomicCpuCycles / (double)testedElements
       << std::setw(20) << ompAtomicOptCpuCycles / (double)testedElements
       << std::setw(25) << ompFalseSharingCpuCycles / (double)testedElements
       << std::setw(25) << ompNoFalseSharingCpuCycles / (double)testedElements
       << std::setw(20) << ompReductionCpuCycles / (double)testedElements
       << std::endl;
}

int main(int argc, char **argv)
{
   OMPTestConfig config;
   if (!config.parseCommandLineArguments(argc, argv))
      return EXIT_FAILURE;

   /****
    * Open the output file for the test results
    */
   std::fstream outputFile;
   if (config.outputFile)
   {
      outputFile.open(config.outputFile, std::ios::out);
      if (!outputFile)
      {
         std::cerr << "I am not able to open the test output file " << config.outputFile << std::endl;
         return EXIT_FAILURE;
      }
   }
   if (config.verbose)
      writeTableHeader(std::cout);
   writeTableHeader(outputFile);
   const int initialVectorSize = config.initialVectorSize;
   const int maxVectorSize = config.maxVectorSize;
   omp_set_num_threads(config.numThreads);
   CPUCyclesCounter cpuCycles;
   TimerRT timer;
   int sequentialResult(0.0),
       ompWrongResult(0.0),
       ompAtomicResult(0.0),
       ompAtomicOptResult(0.0),
       ompFalseSharingResult(0.0),
       ompNoFalseSharingResult(0.0),
       ompReductionResult(0.0);
   for (int vectorSize = initialVectorSize;
        vectorSize <= maxVectorSize;
        vectorSize *= 2)
   {
      int *v = new int[vectorSize];
      for (int i = 0; i < vectorSize; i++)
         v[i] = i;

      unsigned long long int testedElements(0);

      /****
       * Sequential test
       */
      cpuCycles.reset();
      timer.reset();
      timer.start();
      cpuCycles.start();
      while (testedElements < config.elementsPerTest)
      {
         sequentialResult += maxSequential(v, vectorSize);
         testedElements += vectorSize;
      }
      cpuCycles.stop();
      timer.stop();
      unsigned long long sequentialCycles = cpuCycles.getCycles();
       */
      cpuCycles.reset();
      timer.reset();
      timer.start();
      cpuCycles.start();
      while (testedElements < config.elementsPerTest)
      {
         ompWrongResult += maxOmpWrong(v, vectorSize);
         testedElements += vectorSize;
      }
      cpuCycles.stop();
      timer.stop();
      unsigned long long ompWrongCycles = cpuCycles.getCycles();

      /****
       * OpenMP atomic test
       */
      testedElements = 0;
      cpuCycles.reset();
      timer.reset();
      timer.start();
      cpuCycles.start();
      while (testedElements < config.elementsPerTest)
      {
         ompAtomicResult += maxOmpAtomic(v, vectorSize);
         testedElements += vectorSize;
      }
      cpuCycles.stop();
      timer.stop();
      unsigned long long ompAtomicCycles = cpuCycles.getCycles();

      /****
       * OpenMP atomic optimized test
       */
      testedElements = 0;
      cpuCycles.reset();
      timer.reset();
      timer.start();
      cpuCycles.start();
      while (testedElements < config.elementsPerTest)
      {
         ompAtomicOptResult += maxOmpAtomicOpt(v, vectorSize);
         testedElements += vectorSize;
      }
      cpuCycles.stop();
      timer.stop();
      unsigned long long ompAtomicOptCycles = cpuCycles.getCycles();

      /****
       * OpenMP false sharing
       */
      testedElements = 0;
      cpuCycles.reset();
      timer.reset();
      timer.start();
      cpuCycles.start();
      while (testedElements < config.elementsPerTest)
      {
         ompFalseSharingResult += maxOmpFalseSharing(v1, vectorSize);
      }
      timer.stop();
      unsigned long long ompFalseSharingCycles = cpuCycles.getCycles();

      /****
       * OpenMP no false sharing
       */
      testedElements = 0;
      cpuCycles.reset();
      timer.reset();
      timer.start();
      cpuCycles.start();
      while (testedElements < config.elementsPerTest)
      {
         ompNoFalseSharingResult += maxOmpNoFalseSharing(v, vectorSize);
         testedElements += vectorSize;
      }
      cpuCycles.stop();
      timer.stop();
      unsigned long long ompNoFalseSharingCycles = cpuCycles.getCycles();

      /****
       * OpenMP reduction
       */
      testedElements = 0;
      cpuCycles.reset();
      timer.reset();
      timer.start();
      cpuCycles.start();
      while (testedElements < config.elementsPerTest)
      {
         ompReductionResult += maxOmpReduction(v, vectorSize);
         testedElements += vectorSize;
      }
      cpuCycles.stop();
      timer.stop();
      unsigned long long ompReductionCycles = cpuCycles.getCycles();

      if (config.verbose)
         writeTableLine(std::cout, vectorSize, testedElements,
                        sequentialCycles, ompWrongCycles, ompAtomicCycles, ompAtomicOptCycles, ompFalseSharingCycles, ompNoFalseSharingCycles, ompReductionCycles);
      writeTableLine(outputFile, vectorSize, testedElements,
                     sequentialCycles, ompWrongCycles, ompAtomicCycles, ompAtomicOptCycles, ompFalseSharingCycles, ompNoFalseSharingCycles, ompReductionCycles);
      delete[] v;
   }
   std::cout << "Sequential result:           " << sequentialResult << std::endl
             << "OMP wrong result:            " << ompWrongResult << std::endl
             << "OMP atomic result:           " << ompAtomicResult << std::endl
             << "OMP atomic optimized result: " << ompAtomicResult << std::endl
             << "OMP false-sharing result:    " << ompFalseSharingResult << std::endl
             << "OMP no false-sharing result: " << ompNoFalseSharingResult << std::endl
             << "OMP reduction result:        " << ompReductionResult << std::endl;
}