#include <omp.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <CPUCyclesCounter.h>
#include <TimerRT.h>
#include <PapiCounter.h>
#include <cstring>
#include "OMPTestConfig.h"

double scalarProductSequential(const double *v1, const double *v2, const int size)
{
   double res(0.0);
   for (int i = 0; i < size; i++)
      res += v1[i] * v2[i];
   return res;
}

double scalarProductParallelStatic(const double *v1, const double *v2, const int size)
{
   double res(0.0);
#pragma omp parallel for reduction(+ : res), schedule(static), firstprivate(v1, v2)
   for (int i = 0; i < size; i++)
   {
      res += v1[i] * v2[i];
   }
   return res;
}

double scalarProductParallelDynamic(const double *v1, const double *v2, const int size)
{
   double res(0.0);
#pragma omp parallel for reduction(+ : res), schedule(dynamic), firstprivate(v1, v2)
   for (int i = 0; i < size; i++)
      res += v1[i] * v2[i];
   return res;
}

void writeTableHeader(std::ostream &str)
{
   str << std::right;
   str << "#";
   str << std::setw(220) << std::setfill('-') << "-" << std::endl;
   str << "#";
   str << std::setfill(' ');
   str << std::setw(20) << "Vector size"
       << std::setw(20) << "log2 vector size"
       << std::setw(50) << "Sequential               "
       << std::setw(65) << "Parallel                 " << std::endl;
   str << "#";
   str << std::setw(40) << ""
       << std::setw(20) << "Time"
       << std::setw(20) << "CPU tics"
       << std::setw(20) << "M. Bandwidth"
       << std::setw(20) << "L1 Cache eff."
       << std::setw(20) << "Time"
       << std::setw(20) << "CPU tics"
       << std::setw(20) << "Speed-up"
       << std::setw(20) << "M. Bandwidth"
       << std::setw(20) << "L1 Cache eff." << std::endl;
   str << "#";
   str << std::setw(20) << "[els.]"
       << std::setw(20) << "[log2(els.)]"
       << std::setw(20) << "[mu-s./el.]"
       << std::setw(20) << "[tics/el.]"
       << std::setw(20) << "[GB/sec]"
       << std::setw(20) << "[%]"
       << std::setw(20) << "[mu-s./el/]"
       << std::setw(20) << "[tics/el.]"
       << std::setw(20) << ""
       << std::setw(20) << "[GB/sec]"
       << std::setw(20) << "[%]"
       << std::endl;
   str << "#";
   str << std::setw(220) << std::setfill('-') << "-" << std::endl;
   str << std::setfill(' ');
}

void writeTableLine(std::ostream &str,
                    const unsigned long long int size,
                    const unsigned long long int testedElements,
                    const double &sequentialTime,
                    const unsigned long long int sequentialCpuCycles,
                    const double &sequentialL1CacheEfficiency,
                    const double &parallelTime,
                    const unsigned long long int parallelCpuCycles,
                    const double &parallelL1CacheEfficiency)
{
   double sequentialBandwidth = 2 * sizeof(double) * testedElements / sequentialTime / 1.0e9;
   double parallelBandwidth = 2 * sizeof(double) * testedElements / parallelTime / 1.0e9;

   str << std::setw(20) << size
       << std::setw(20) << log2(size)
       << std::setw(20) << sequentialTime / (double)testedElements
       << std::setw(20) << sequentialCpuCycles / (double)testedElements
       //<< std::setw( 20 ) << log10( sequentialCpuCycles / ( double ) testedElements )
       << std::setw(20) << sequentialBandwidth
       << std::setw(20) << 100.0 * sequentialL1CacheEfficiency

       << std::setw(20) << parallelTime / (double)testedElements
       << std::setw(20) << parallelCpuCycles / (double)testedElements
       << std::setw(20) << sequentialTime / parallelTime
       //<< std::setw( 20 ) << log10( sequentialCpuCycles / ( double ) testedElements )
       << std::setw(20) << parallelBandwidth
       << std::setw(20) << 100.0 * parallelL1CacheEfficiency << std::endl;
}

int main(int argc, char **argv)
{
   OMPTestConfig config;
   if (!config.parseCommandLineArguments(argc, argv))
      return EXIT_FAILURE;

   /****
    * Open the output file for the test results
    */
   std::fstream outputFile;
   if (config.outputFile)
   {
      outputFile.open(config.outputFile, std::ios::out);
      if (!outputFile)
      {
         std::cerr << "I am not able to open the test output file " << config.outputFile << std::endl;
         return EXIT_FAILURE;
      }
   }
   if (config.verbose)
      writeTableHeader(std::cout);
   writeTableHeader(outputFile);
   const int initialVectorSize = config.initialVectorSize;
   const int maxVectorSize = config.maxVectorSize;
   omp_set_num_threads(config.numThreads);
   CPUCyclesCounter cpuCycles;
   TimerRT timer;
   PapiCounter papiCounter;
#ifdef HAVE_PAPI
   papiCounter.setNumberOfEvents(2);
   papiCounter.addEvent(PAPI_L1_DCA);
   papiCounter.addEvent(PAPI_L1_DCH);
#endif
   double sequentialResult(0.0), parallelResult(0.0);
   for (int vectorSize = initialVectorSize;
        vectorSize <= maxVectorSize;
        vectorSize *= 2)
   {
      double *v1 = new double[vectorSize];
      double *v2 = new double[vectorSize];
      for (int i = 0; i < vectorSize; i++)
         v1[i] = v2[i] = 1.0;
      unsigned long long int testedElements(0);

      /****
       * Sequential test
       */
      cpuCycles.reset();
      timer.reset();
      papiCounter.reset();
      timer.start();
      cpuCycles.start();
      papiCounter.start();
      while (testedElements < config.elementsPerTest)
      {
         sequentialResult += scalarProductSequential(v1, v2, vectorSize);
         testedElements += vectorSize;
      }
      cpuCycles.stop();
      timer.stop();
      papiCounter.stop();
      unsigned long long sequentialCycles = cpuCycles.getCycles();
      double sequentialTime = timer.getTime();
      double sequentialL1CacheEfficiency(-1.0);
#ifdef HAVE_PAPI
      sequentialL1CacheEfficiency = (double)papiCounter.getEventValue(PAPI_L1_DCH) / (double)papiCounter.getEventValue(PAPI_L1_DCA);
#endif

      /****
       * Parallel static test
       */
      testedElements = 0;
      double parallelStaticResult(0.0);
      cpuCycles.reset();
      timer.reset();
      papiCounter.reset();
      if (strcmp(config.testType, "static") == 0)
      {
         timer.start();
         cpuCycles.start();
         papiCounter.start();
         while (testedElements < config.elementsPerTest)
         {
            parallelResult += scalarProductParallelStatic(v1, v2, vectorSize);
            testedElements += vectorSize;
         }
         cpuCycles.stop();
         timer.stop();
         papiCounter.stop();
      }
      else
      {
         timer.start();
         cpuCycles.start();
         papiCounter.start();
         while (testedElements < config.elementsPerTest)
         {
            parallelResult += scalarProductParallelStatic(v1, v2, vectorSize);
            testedElements += vectorSize;
         }
         cpuCycles.stop();
         timer.stop();
         papiCounter.stop();
      }
      unsigned long long parallelStaticCycles = cpuCycles.getCycles();
      double parallelStaticTime = timer.getTime();
      double parallelStaticL1CacheEfficiency(-1.0);
#ifdef HAVE_PAPI
      parallelStaticL1CacheEfficiency = (double)papiCounter.getEventValue(PAPI_L1_DCH) / (double)papiCounter.getEventValue(PAPI_L1_DCA);
#endif
      if (config.verbose)
         writeTableLine(std::cout, vectorSize, testedElements,
                        sequentialTime, sequentialCycles, sequentialL1CacheEfficiency,
                        parallelStaticTime, parallelStaticCycles, parallelStaticL1CacheEfficiency);
      writeTableLine(outputFile, vectorSize, testedElements,
                     sequentialTime, sequentialCycles, sequentialL1CacheEfficiency,
                     parallelStaticTime, parallelStaticCycles, parallelStaticL1CacheEfficiency);
      delete[] v1;
      delete[] v2;
   }
   std::cout << sequentialResult << " " << parallelResult << std::endl;
}