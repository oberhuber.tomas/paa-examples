#include <omp.h>
#include <stdlib.h>
#include <iostream>

int main( int argc, char** argv )
{
   int shared_int( 1 ), private_int( 1 ), firstprivate_int( 1 );
#pragma omp parallel shared( shared_int ), private( private_int ), firstprivate( firstprivate_int )
   {
#pragma omp critical
      std::cout << "Variables of thread " 
                << omp_get_thread_num() << " are"
                << " shared_int = " << shared_int
                << " private_int = " << private_int
                << " firstprivate_int = " << firstprivate_int
                << std::endl;
      
#pragma omp barrier
#pragma omp atomic
      shared_int ++;
      private_int ++;
      firstprivate_int ++;
#pragma omp barrier      
#pragma omp single
      std::cout << "=======================================" << std::endl;
#pragma omp critical
      std::cout << "Variables of thread " 
                << omp_get_thread_num() << " are"
                << " shared_int = " << shared_int
                << " private_int = " << private_int
                << " firstprivate_int = " << firstprivate_int
                << std::endl;
      
   }
   return EXIT_SUCCESS;
}

