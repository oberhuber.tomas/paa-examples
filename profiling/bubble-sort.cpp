/* 
 * File:   bubble-sort.cpp
 * Author: oberhuber
 *
 * Created on February 22, 2016, 12:21 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */

void swap( int& a, int& b )
{
    int c;
    c = a;
    a = b;
    b = c;
}

void bubbleSort( int* data, const int size )
{
    int l( 0 ), r( size );
    while( l < r )
    {
        int lastChange;
        for( int j = l; j < r - 1; j++ )
            if( data[ j ] > data[ j+1 ] )
            {
                swap( data[ j ], data[ j+1 ] );
                lastChange = j;
            }
        r = lastChange;
        for( int j = r - 1; j >= l; j-- )
            if( data[ j ] > data[ j+1 ] )
            {
                swap( data[ j ], data[ j+1 ] );
                lastChange = j;
            }
        l = lastChange + 1;
        std::cout << ( double ) ( size - r + l ) / ( double ) size * 100.0  << " % done ...          \r " << std::flush;
    }
    std::cout << std::endl;
}

int main( int argc, char** argv )
{
    const int size = 10000;
    int* data = new int[ size ];
    for( int i = 0; i < size; i++ )
        data[ i ] = size - i;
    bubbleSort( data, size );
    delete[] data;
    return EXIT_SUCCESS;
}

