/***************************************************************************
                          conditions.cpp  -  description
                             -------------------
    begin                : 2015/02/04
    copyright            : (C) 2015 by Tomáš Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <math.h>
#include <stdlib.h>
#include <CPUCyclesCounter.h>
#include <PapiCounter.h>

using namespace std;

void setupTest( int* data,
                const int size,
                const int patternLength,
                const int n = 1 )
{
   srand( 0 );
   if( patternLength )
   {
      int* pattern = new int[ patternLength ];
      for( int i = 0; i < patternLength; i++ )
         pattern[ i ] = rand() % ( 2 * n + 1 ) - n;
      for( int i = 0; i < size; i++ )
      {
         data[ i ] = pattern[ i % patternLength ];
      }
      delete[] pattern;
   }
   else
      for( int i = 0; i < size; i++ )
         data[ i ] = rand() % 3 - 1;
}

void performTest( const int* data,
                  const int size,
                  int& a )
{
   a = 0;
   for( int i = 0; i < size; i++ )
   {
      if( data[ i ] == 1 )
         a++;
      if( data[ i ] == 0 )
         a = 0;
      if( data[ i ] == -1 )
         a--;
   }
}

void performUnrolledTest( const int* data,
                          const int size,
                          int& a )
{
   a = 0;
   for( int i = 0; i < size; i += 4 )
   {
      if( data[ i ] == 1 )
         a++;
      if( data[ i ] == 0 )
         a = 0;
      if( data[ i ] == -1 )
         a--;

      if( data[ i + 1 ] == 1 )
         a++;
      if( data[ i + 1 ] == 0 )
         a = 0;
      if( data[ i + 1 ] == -1 )
         a--;

      if( data[ i + 2 ] == 1 )
         a++;
      if( data[ i + 2 ] == 0 )
         a = 0;
      if( data[ i + 2 ] == -1 )
         a--;

      if( data[ i + 3 ] == 1 )
         a++;
      if( data[ i + 3 ] == 0 )
         a = 0;
      if( data[ i + 3 ] == -1 )
         a--;
   }
}

void performSwitchTest( const int* data,
                        const int size,
                        int& a )
{
   a = 0;
   for( int i = 0; i < size; i ++ )
   {
      switch( data[ i ] )
      {
         case 1:
            a++;
            break;
         case -1:
            a--;
            break;
         default:
            a = 0;
      }
   }
}

void performUnrolledSwitchTest( const int* data,
                                const int size,
                                int& a )
{
   a = 0;
   for( int i = 0; i < size; i += 4 )
   {
      switch( data[ i ] )
      {
         case 1:
            a++;
            break;
         case -1:
            a--;
            break;
         default:
            a = 0;
      }
      switch( data[ i + 1 ] )
      {
         case 1:
            a++;
            break;
         case -1:
            a--;
            break;
         default:
            a = 0;
      }
      switch( data[ a + 2 ] )
      {
         case 1:
            a++;
            break;
         case -1:
            a--;
            break;
         default:
            a = 0;
      }
      switch( data[ a + 3 ] )
      {
         case 1:
            a++;
            break;
         case -1:
            a--;
            break;
         default:
            a = 0;
      }
   }
}


void performNoConditionsTest( const int* data,
                              const int size,
                              int& a )
{
   a = 0;
   for( int i = 0; i < size; i += 4 )
   {
      a += data[ i ];
      a += data[ i + 1 ];
      a += data[ i + 2 ];
      a += data[ i + 3 ];
   }
}

void performMultivalueTest( const int* data,
                            const int size,
                            const int n,
                            int& a )
{
   a = 0;
   for( int i = 0; i < size; i++ )
   {
      for( int j = -n; j <= n; j++ )
      {
         if( data[ i ] == n )
            a++;
         if( data[ i ] == -n )
            a--;
      }
      if( data[ i ] == 0 )
         a = 0;
   }
}

template< int n >
struct MultivalueTester
{
   static void test( const int data, int& a )
   {
      if( data == n )
         a++;
      if( data == -n )
         a--;
      MultivalueTester< n - 1 >::test( data, a );
   }
};

template<>
struct MultivalueTester< 0 >
{
   static void test( const int data, int& a )
   {
      if( data == 0 )
         a = 0;
   }
};

template< int n >
void performTemplateMultivalueTest( const int* data,
                                    const int size,
                                    int& a )
{
   a = 0;
   for( int i = 0; i < size; i += 4 )
   {
      MultivalueTester< n >::test( data[ i ], a );
      MultivalueTester< n >::test( data[ i + 1 ], a );
      MultivalueTester< n >::test( data[ i + 2 ], a );
      MultivalueTester< n >::test( data[ i + 3 ], a );
   }
}

/*void performSwitchMultivalueTest( const int* data,
                                  const int size,
                                  int& a )
{
   a = 0;
   for( int i = 0; i < size; i++ )
   {
      switch( data[ i ] )
      {
         case 8:
            a++;
            break;
      }
   }
}*/


void writeTableHeader( ostream& str )
{
   str << right;
   str << "#";
   str << std::setw( 120 ) << setfill( '-') << "-" << std::endl;
   str << "#";
   str << setfill( ' ' );
   str << std::setw( 40 ) << "Pattern length             "
       << std::setw( 20 ) << "CPU tics             "
       << std::setw( 40 ) << "Misprediction"
       << std::setw( 20 ) << "Result" << std::endl;
   str << "#";
   str << std::setw( 20 ) << "(elements)"
       << std::setw( 20 ) << "(log2 of elements)"
       << std::setw( 20 ) << "(tics/el.)"
       << std::setw( 20 ) << "(%)"
       << std::setw( 20 ) << "(log10 of %)"
       << std::endl;
   str << "#";
   str << std::setw( 120 ) << setfill( '-') << "-" << std::endl;
   str << setfill( ' ' );
}

void writeTableLine( ostream& str,
                     const long int patternLength,
                     const double& tics,
                     const double& misprediction,
                     int a )
{
   str << std::setw( 20 ) << patternLength
       << std::setw( 20 ) << log2( patternLength )
       << std::setw( 20 ) << tics
       << std::setw( 20 ) <<  misprediction
       << std::setw( 20 ) <<  log10( misprediction )
       << std::setw( 20 ) << a
       << std::endl;
}

void writeTableLastLine( ostream& str )
{
   str << "#";
   str << std::setw( 120 ) << setfill( '-') << "-" << std::endl;
   str << setfill( ' ' );
}


int main( int argc, char* argv[] )
{
   const int size = 2097152;
   const int loops = 4;

   int* data = new int[ size ];

   CPUCyclesCounter cpuCycles;
   PapiCounter papiCounter;
#ifdef HAVE_PAPI
   papiCounter.setNumberOfEvents( 2 );
   papiCounter.addEvent( PAPI_BR_INS ); // Branch instructions
   papiCounter.addEvent( PAPI_BR_MSP ); // Conditional branch instructions mispredicted
#endif
   double misprediction( -1.0 );
   std::fstream file;
   int a( 0 );

   /****
    * Baseline conditions test
    */
   std::cout << "No unrolling..." << std::endl;
   writeTableHeader( std::cout );
   file.open( "conditions-results.txt", ios::out );
   if( ! file )
   {
      std::cerr << "I am not able to open a file conditions-results.txt." << std::endl;
      return EXIT_FAILURE;
   }
   writeTableHeader( file );
   for( int patternLength = 1; patternLength <= size; patternLength *= 2 )
   {
      setupTest( data, size, patternLength );

      cpuCycles.reset();
      cpuCycles.start();
      papiCounter.reset();
      papiCounter.start();
      for( int i = 0; i < loops; i++ )
         performTest( data, size, a );
      cpuCycles.stop();
      papiCounter.stop();
#ifdef HAVE_PAPI
      misprediction = 100.0 * ( double ) papiCounter.getEventValue( PAPI_BR_MSP ) / ( double ) papiCounter.getEventValue( PAPI_BR_INS );
#endif
      writeTableLine( std::cout, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
      writeTableLine( file, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );

   }
   writeTableLastLine( std::cout );
   writeTableLastLine( file );
   file.close();

   /****
    * Conditions test with unrolling
    */
   std::cout << "With unrolling..." << std::endl;
   writeTableHeader( std::cout );
   file.open( "unrolled-conditions-results.txt", ios::out );
   if( ! file )
   {
      std::cerr << "I am not able to open a file unrolled-conditions-results.txt." << std::endl;
      return EXIT_FAILURE;
   }
   writeTableHeader( file );
   for( int patternLength = 1; patternLength <= size; patternLength *= 2 )
   {
      setupTest( data, size, patternLength );
      cpuCycles.reset();
      cpuCycles.start();
      papiCounter.reset();
      papiCounter.start();

      for( int i = 0; i < loops; i++ )
         performUnrolledTest( data, size, a );
      cpuCycles.stop();
      papiCounter.stop();
#ifdef HAVE_PAPI
      misprediction = 100.0 * ( double ) papiCounter.getEventValue( PAPI_BR_MSP ) / ( double ) papiCounter.getEventValue( PAPI_BR_INS );
#endif
      writeTableLine( std::cout, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
      writeTableLine( file, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
   }
   writeTableLastLine( std::cout );
   writeTableLastLine( file );
   file.close();

   /****
    * Conditions test with unrolling and blocks
    */
   std::cout << "With unrolling and blocks..." << std::endl;
   writeTableHeader( std::cout );
   file.open( "blocked-unrolled-conditions-results.txt", ios::out );
   if( ! file )
   {
      std::cerr << "I am not able to open a file blocked-unrolled-conditions-results.txt." << std::endl;
      return EXIT_FAILURE;
   }
   writeTableHeader( file );
   for( int patternLength = 1; patternLength <= size; patternLength *= 2 )
   {
      setupTest( data, size, patternLength );
      cpuCycles.reset();
      cpuCycles.start();
      papiCounter.reset();
      papiCounter.start();

      const int blockSize = 32;
      for( int offset = 0; offset < size; offset += blockSize )
         for( int i = 0; i < loops; i++ )
            performUnrolledTest( &data[ offset ], blockSize, a );
      cpuCycles.stop();
      papiCounter.stop();
#ifdef HAVE_PAPI
      misprediction = 100.0 * ( double ) papiCounter.getEventValue( PAPI_BR_MSP ) / ( double ) papiCounter.getEventValue( PAPI_BR_INS );
#endif
      writeTableLine( std::cout, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
      writeTableLine( file, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
   }
   writeTableLastLine( std::cout );
   writeTableLastLine( file );
   file.close();

   /****
    * Test with switch
    */
   std::cout << "With switch..." << std::endl;
   writeTableHeader( std::cout );
   file.open( "switch-conditions-results.txt", ios::out );
   if( ! file )
   {
      std::cerr << "I am not able to open a file switch-conditions-results.txt." << std::endl;
      return EXIT_FAILURE;
   }
   writeTableHeader( file );
   for( int patternLength = 1; patternLength <= size; patternLength *= 2 )
   {
      setupTest( data, size, patternLength );
      cpuCycles.reset();
      cpuCycles.start();
      papiCounter.reset();
      papiCounter.start();

      const int blockSize = 32;
      for( int offset = 0; offset < size; offset += blockSize )
         for( int i = 0; i < loops; i++ )
            performSwitchTest( &data[ offset ], blockSize, a );

      cpuCycles.stop();
      papiCounter.stop();
#ifdef HAVE_PAPI
      misprediction = 100.0 * ( double ) papiCounter.getEventValue( PAPI_BR_MSP ) / ( double ) papiCounter.getEventValue( PAPI_BR_INS );
#endif
      writeTableLine( std::cout, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
      writeTableLine( file, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
   }
   writeTableLastLine( std::cout );
   writeTableLastLine( file );
   file.close();

   /****
    * Test with unrolled switch
    */
   std::cout << "With unrolled switch..." << std::endl;
   writeTableHeader( std::cout );
   file.open( "unrolled-switch-conditions-results.txt", ios::out );
   if( ! file )
   {
      std::cerr << "I am not able to open a file unrolled-switch-conditions-results.txt." << std::endl;
      return EXIT_FAILURE;
   }
   writeTableHeader( file );
   for( int patternLength = 1; patternLength <= size; patternLength *= 2 )
   {
      setupTest( data, size, patternLength );
      cpuCycles.reset();
      cpuCycles.start();
      papiCounter.reset();
      papiCounter.start();

      const int blockSize = 32;
      for( int offset = 0; offset < size; offset += blockSize )
         for( int i = 0; i < loops; i++ )
            performUnrolledSwitchTest( &data[ offset ], blockSize, a );

      cpuCycles.stop();
      papiCounter.stop();
#ifdef HAVE_PAPI
      misprediction = 100.0 * ( double ) papiCounter.getEventValue( PAPI_BR_MSP ) / ( double ) papiCounter.getEventValue( PAPI_BR_INS );
#endif
      writeTableLine( std::cout, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
      writeTableLine( file, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
   }
   writeTableLastLine( std::cout );
   writeTableLastLine( file );
   file.close();


   /****
    * Test with no conditions (to see the effect of the conditions)
    */
   std::cout << "With no conditions..." << std::endl;
   writeTableHeader( std::cout );
   file.open( "no-conditions-results.txt", ios::out );
   if( ! file )
   {
      std::cerr << "I am not able to open a file no-conditions-results.txt." << std::endl;
      return EXIT_FAILURE;
   }
   writeTableHeader( file );
   for( int patternLength = 1; patternLength <= size; patternLength *= 2 )
   {
      setupTest( data, size, patternLength );
      cpuCycles.reset();
      cpuCycles.start();
      papiCounter.reset();
      papiCounter.start();

      for( int i = 0; i < loops; i++ )
         performNoConditionsTest( data, size, a );
      cpuCycles.stop();
      papiCounter.stop();
#ifdef HAVE_PAPI
      misprediction = 100.0 * ( double ) papiCounter.getEventValue( PAPI_BR_MSP ) / ( double ) papiCounter.getEventValue( PAPI_BR_INS );
#endif
      writeTableLine( std::cout, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
      writeTableLine( file, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
   }
   writeTableLastLine( std::cout );
   writeTableLastLine( file );
   file.close();


   /****
    *
    *               MULTIVALUED TEST
    *
    */
   for( int n = 2; n <= 32; n *= 2 )
   {
      std::cout << "Multivalue test with n = " << n << "..." << std::endl;
      writeTableHeader( std::cout );

      stringstream fileName;
      fileName << n << "-value-test-results.txt";
      file.open( fileName.str(), ios::out );
      if( ! file )
      {
         std::cerr << "I am not able to open a file " << fileName.str() << "." << std::endl;
         return EXIT_FAILURE;
      }
      writeTableHeader( file );
      for( int patternLength = 1; patternLength <= size; patternLength *= 2 )
      {
         setupTest( data, size, patternLength, n );
         cpuCycles.reset();
         cpuCycles.start();
         papiCounter.reset();
         papiCounter.start();

         for( int i = 0; i < loops; i++ )
            performMultivalueTest( data, size, n, a );

         cpuCycles.stop();
         papiCounter.stop();
   #ifdef HAVE_PAPI
         misprediction = 100.0 * ( double ) papiCounter.getEventValue( PAPI_BR_MSP ) / ( double ) papiCounter.getEventValue( PAPI_BR_INS );
   #endif
         writeTableLine( std::cout, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
         writeTableLine( file, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
      }
      writeTableLastLine( std::cout );
      writeTableLastLine( file );
      file.close();

      std::cout << "Multivalue blocked test with n = " << n << "..." << std::endl;
      writeTableHeader( std::cout );

      fileName.str("");
      fileName << n << "-value-blocked-test-results.txt";
      file.open( fileName.str(), ios::out );
      if( ! file )
      {
         std::cerr << "I am not able to open a file " << fileName.str() << "." << std::endl;
         return EXIT_FAILURE;
      }
      writeTableHeader( file );
      for( int patternLength = 1; patternLength <= size; patternLength *= 2 )
      {
         setupTest( data, size, patternLength, n );
         cpuCycles.reset();
         cpuCycles.start();
         papiCounter.reset();
         papiCounter.start();

         const int blockSize = 32;
         for( int offset = 0; offset < size; offset += blockSize )
            for( int i = 0; i < loops; i++ )
               performMultivalueTest( &data[ offset ], blockSize, n, a );

         cpuCycles.stop();
         papiCounter.stop();
   #ifdef HAVE_PAPI
         misprediction = 100.0 * ( double ) papiCounter.getEventValue( PAPI_BR_MSP ) / ( double ) papiCounter.getEventValue( PAPI_BR_INS );
   #endif
         writeTableLine( std::cout, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
         writeTableLine( file, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
      }
      writeTableLastLine( std::cout );
      writeTableLastLine( file );
      file.close();

      std::cout << "Multivalue template test with n = " << n << "..." << std::endl;
      writeTableHeader( std::cout );

      fileName.str("");
      fileName << n << "-value-template-test-results.txt";
      file.open( fileName.str(), ios::out );
      if( ! file )
      {
         std::cerr << "I am not able to open a file " << fileName.str() << "." << std::endl;
         return EXIT_FAILURE;
      }
      writeTableHeader( file );
      for( int patternLength = 1; patternLength <= size; patternLength *= 2 )
      {
         setupTest( data, size, patternLength, n );
         cpuCycles.reset();
         cpuCycles.start();
         papiCounter.reset();
         papiCounter.start();

         const int blockSize = 32;
         for( int offset = 0; offset < size; offset += blockSize )
            for( int i = 0; i < loops; i++ )
            {
               switch( n )
               {
                  case 2:
                     performTemplateMultivalueTest< 2 >( &data[ offset ], blockSize, a );
                     break;
                  case 4:
                     performTemplateMultivalueTest< 4 >( &data[ offset ], blockSize, a );
                     break;
                  case 8:
                     performTemplateMultivalueTest< 8 >( &data[ offset ], blockSize, a );
                     break;
                  case 16:
                     performTemplateMultivalueTest< 16 >( &data[ offset ], blockSize, a );
                     break;
                  case 32:
                     performTemplateMultivalueTest< 32 >( &data[ offset ], blockSize, a );
                     break;

               }
            }
         cpuCycles.stop();
         papiCounter.stop();
   #ifdef HAVE_PAPI
         misprediction = 100.0 * ( double ) papiCounter.getEventValue( PAPI_BR_MSP ) / ( double ) papiCounter.getEventValue( PAPI_BR_INS );
   #endif
         writeTableLine( std::cout, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
         writeTableLine( file, patternLength,( double ) cpuCycles.getCycles() / ( double ) ( loops * size ), misprediction, a );
      }
      writeTableLastLine( std::cout );
      writeTableLastLine( file );
      file.close();

   }

   delete[] data;
   return EXIT_SUCCESS;
}


