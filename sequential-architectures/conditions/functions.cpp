/***************************************************************************
                          functions.cpp  -  description
                             -------------------
    begin                : Feb 25, 2015
    copyright            : (C) 2015 by Tomáš Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <TNL/Benchmarks/Benchmarks.h>
#include <TNL/Config/ConfigDescription.h>
#include <TNL/Config/parseCommandLine.h>

int data[ 4 ];

class adder
{
   public:

      virtual void addNumber( int& n ) const = 0;
};

class oneAdder : public adder
{
   public:

      void addNumber( int& n ) const { n += ::data[ 0 ]; ::data[ 0 ] *= -1; };
};

template< int index >
class templateAdder
{
   public:
      void addNumber( int& n ) const { n += ::data[ index ]; ::data[ index ] *= -1; };

};

class switchAdder
{
   public:

      switchAdder( int c )
      : number( c ){};

      void addNumber( int& n ) const
      {
         switch( this->number )
         {
             case 0:
                n += ::data[ 0 ];
                break;
             case 1:
                n += ::data[ 1 ];
                break;
             case 2:
                n += ::data[ 2 ];
                break;
             case 3:
                n += ::data[ 3 ];
                break;
         }
         ::data[ this->number ] *= -1;
      }

   protected:

      int number;
};

class inlineAdder
{
   public:

      inline void addNumber( int& n ) const { n += ::data[ 0 ]; ::data[ 0 ] *= -1; };
};

void configSetup( TNL::Config::ConfigDescription& config )
{
   config.addDelimiter("Benchmark settings:");
   config.addEntry<TNL::String>("log-file", "Log file name.", "functions-calls.log");
   config.addEntry<TNL::String>("output-mode", "Mode for opening the log file.", "overwrite");
   config.addEntryEnum("overwrite");
   config.addEntryEnum("append");
   config.addEntry< bool >( "verbose", "Verbose mode.", true );
}


int main( int argc, char* argv[] )
{
   TNL::Config::ConfigDescription config;
   configSetup( config );
   TNL::Config::ParameterContainer parameters;
   if( !TNL::Config::parseCommandLine( argc, argv, config, parameters ) )
      return EXIT_FAILURE;

   const auto verbose = parameters.getParameter< bool >( "verbose" );
   const auto log_file_name = parameters.getParameter< TNL::String >( "log-file" );
   const auto output_mode = parameters.getParameter< TNL::String >( "output-mode" );
   auto mode = std::ios::out;
   if( output_mode == "append" )
      mode |= std::ios::app;
   std::ofstream log_file( log_file_name.getString(), mode );

   const unsigned  long long int loops = 1 << 15;
   TNL::Benchmarks::Benchmark<> benchmark( log_file, loops, verbose );
   benchmark.setOperationsPerLoop( loops );

   // write global metadata into a separate file
   std::map< std::string, std::string > metadata = TNL::Benchmarks::getHardwareMetadata();
   TNL::Benchmarks::writeMapAsJson( metadata, log_file_name, ".metadata.json" );

   for( int i = 0; i < 4; i++ )
      ::data[ i ] = i+1;
   int sum( 0 );

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "call", "direct", }
   }));
   oneAdder one_adder;
   auto f_one_adder = [&] () mutable {
      for( unsigned long long int i = 0; i < loops; i++ )
         one_adder.addNumber( sum );
   };
#ifdef __APPLE__
   benchmark.time< TNL::Devices::Sequential >( "sequential", f_one_adder );
#endif
   benchmark.time< TNL::Devices::Sequential >( "sequential", f_one_adder );

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "call", "virtual", }
   }));
   adder* _adder = &one_adder;
   sum = 0;
   auto f_virtual_adder = [&] () mutable {
      for( unsigned long long int i = 0; i < loops; i++ )
         _adder->addNumber( sum );
   };
   benchmark.time< TNL::Devices::Sequential >( "sequential", f_virtual_adder );

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "call", "template", }
   }));
   templateAdder< 1 > template_adder;
   sum = 0;
   auto f_template_adder = [&] () mutable {
      for( unsigned long long int i = 0; i < loops; i++ )
         template_adder.addNumber( sum );
   };
   benchmark.time< TNL::Devices::Sequential >( "sequential", f_template_adder );

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "call", "switch", }
   }));
   switchAdder switch_adder( 1 );
   sum = 0;
   auto f_switch_adder = [&] () mutable {
      for( unsigned long long int i = 0; i < loops; i++ )
         switch_adder.addNumber( sum );
   };
   benchmark.time< TNL::Devices::Sequential >( "sequential", f_switch_adder );

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "call", "inline", }
   }));
   inlineAdder inline_adder;
   sum = 0;
   auto f_inline_adder = [&] () mutable {
      for( unsigned long long int i = 0; i < loops; i++ )
         inline_adder.addNumber( sum );
   };
   benchmark.time< TNL::Devices::Sequential >( "sequential", f_inline_adder );

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "call", "no-call", }
   }));
   sum = 0;
   auto f_no_call_adder = [&] () mutable {
      for( unsigned long long int i = 0; i < loops; i++ )
      {
         sum += ::data[ 0 ];
         ::data[ 0 ] *= -1;
      }
   };
   benchmark.time< TNL::Devices::Sequential >( "sequential", f_no_call_adder );
}
