/***************************************************************************
                          loop-unrolling.cpp  -  description
                             -------------------
    begin                : Feb 25, 2015
    copyright            : (C) 2015 by Tomáš Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <CPUCyclesCounter.h>

using namespace std;

template< int n, int unrolling = n >
class sumUnroller
{
   public:
      static inline void sum( int* __restrict__ data, int& sum )
      {
         sum += data[ unrolling - n ];
         sumUnroller< n - 1, unrolling >::sum( data, sum );
      }
};

template< int unrolling >
class sumUnroller< 1, unrolling >
{
   public:
      static inline void sum( int* __restrict__ data, int& sum )
      {
         sum += data[ unrolling - 1 ];
      }
};

int main( int argc, char* argv[] )
{
   const int size = 2048;

   int data[ size ];
   for( int i = 0; i < size; i++ )
      data[ i ] = 1;
   const int loops = 16384;
   CPUCyclesCounter cpuCycles;
   int sum( 0 );

   /*****
    *  C style unrolling
    */
   std::cout << std::endl << "C style unrolling..." << std::endl;
   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i++ )
         sum += data[ i ];
         //sumUnroller< 1 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "No unrolling: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum << std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i+= 2 )
      {
         sum += data[ i ];
         sum += data[ i + 1 ];
      }
         //sumUnroller< 2 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 2: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i += 4 )
      {
         sum += data[ i ];
         sum += data[ i + 1 ];
         sum += data[ i + 2 ];
         sum += data[ i + 3 ];
      }
      //   sumUnroller< 4 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 4: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i += 8 )
      {
         sum += data[ i ];
         sum += data[ i + 1 ];
         sum += data[ i + 2 ];
         sum += data[ i + 3 ];
         sum += data[ i + 4 ];
         sum += data[ i + 5 ];
         sum += data[ i + 6 ];
         sum += data[ i + 7 ];
      }
      //sumUnroller< 8 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 8: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i += 16 )
      {
         sum += data[ i ];
         sum += data[ i + 1 ];
         sum += data[ i + 2 ];
         sum += data[ i + 3 ];
         sum += data[ i + 4 ];
         sum += data[ i + 5 ];
         sum += data[ i + 6 ];
         sum += data[ i + 7 ];
         sum += data[ i + 8 ];
         sum += data[ i + 9 ];
         sum += data[ i + 10 ];
         sum += data[ i + 11 ];
         sum += data[ i + 12 ];
         sum += data[ i + 13 ];
         sum += data[ i + 14 ];
         sum += data[ i + 15 ];
      }
      //   sumUnroller< 16 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 16: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i += 32 )
      {
         sum += data[ i ];
         sum += data[ i + 1 ];
         sum += data[ i + 2 ];
         sum += data[ i + 3 ];
         sum += data[ i + 4 ];
         sum += data[ i + 5 ];
         sum += data[ i + 6 ];
         sum += data[ i + 7 ];
         sum += data[ i + 8 ];
         sum += data[ i + 9 ];
         sum += data[ i + 10 ];
         sum += data[ i + 11 ];
         sum += data[ i + 12 ];
         sum += data[ i + 13 ];
         sum += data[ i + 14 ];
         sum += data[ i + 15 ];
         sum += data[ i + 16 ];
         sum += data[ i + 17 ];
         sum += data[ i + 18 ];
         sum += data[ i + 19 ];
         sum += data[ i + 20 ];
         sum += data[ i + 21 ];
         sum += data[ i + 22 ];
         sum += data[ i + 23 ];
         sum += data[ i + 24 ];
         sum += data[ i + 25 ];
         sum += data[ i + 26 ];
         sum += data[ i + 27 ];
         sum += data[ i + 28 ];
         sum += data[ i + 29 ];
         sum += data[ i + 30 ];
         sum += data[ i + 31 ];
      }
      //   sumUnroller< 16 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 32: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i += 64 )
      {
         sum += data[ i ];
         sum += data[ i + 1 ];
         sum += data[ i + 2 ];
         sum += data[ i + 3 ];
         sum += data[ i + 4 ];
         sum += data[ i + 5 ];
         sum += data[ i + 6 ];
         sum += data[ i + 7 ];
         sum += data[ i + 8 ];
         sum += data[ i + 9 ];
         sum += data[ i + 10 ];
         sum += data[ i + 11 ];
         sum += data[ i + 12 ];
         sum += data[ i + 13 ];
         sum += data[ i + 14 ];
         sum += data[ i + 15 ];
         sum += data[ i + 16 ];
         sum += data[ i + 17 ];
         sum += data[ i + 18 ];
         sum += data[ i + 19 ];
         sum += data[ i + 20 ];
         sum += data[ i + 21 ];
         sum += data[ i + 22 ];
         sum += data[ i + 23 ];
         sum += data[ i + 24 ];
         sum += data[ i + 25 ];
         sum += data[ i + 26 ];
         sum += data[ i + 27 ];
         sum += data[ i + 28 ];
         sum += data[ i + 29 ];
         sum += data[ i + 30 ];
         sum += data[ i + 31 ];
         sum += data[ i + 32 ];
         sum += data[ i + 33 ];
         sum += data[ i + 34 ];
         sum += data[ i + 35 ];
         sum += data[ i + 36 ];
         sum += data[ i + 37 ];
         sum += data[ i + 38 ];
         sum += data[ i + 39 ];
         sum += data[ i + 40 ];
         sum += data[ i + 41 ];
         sum += data[ i + 42 ];
         sum += data[ i + 43 ];
         sum += data[ i + 44 ];
         sum += data[ i + 45 ];
         sum += data[ i + 46 ];
         sum += data[ i + 47 ];
         sum += data[ i + 48 ];
         sum += data[ i + 49 ];
         sum += data[ i + 50 ];
         sum += data[ i + 51 ];
         sum += data[ i + 52 ];
         sum += data[ i + 53 ];
         sum += data[ i + 54 ];
         sum += data[ i + 55 ];
         sum += data[ i + 56 ];
         sum += data[ i + 57 ];
         sum += data[ i + 58 ];
         sum += data[ i + 59 ];
         sum += data[ i + 60 ];
         sum += data[ i + 61 ];
         sum += data[ i + 62 ];
         sum += data[ i + 63 ];
      }
      //   sumUnroller< 16 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 64: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

   /*****
    *  C++ templates style unrolling
    */
   std::cout << std::endl << "C++ templates unrolling..." << std::endl;
   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i++ )
         sumUnroller< 1 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "No unrolling: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum << std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i+= 2 )
         sumUnroller< 2 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 2: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i += 4 )
         sumUnroller< 4 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 4: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i += 8 )
         sumUnroller< 8 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 8: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i += 16 )
         sumUnroller< 16 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 16: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i += 32 )
         sumUnroller< 32 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 32: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   sum = 0;
   for( int l = 0; l < loops; l++ )
   {
      for( int i = 0; i < size; i += 64 )
         sumUnroller< 64 >::sum( &data[ i ], sum );
   }
   cpuCycles.stop();
   std::cout << "Unrolling 64: CPU clocks per element " << ( double ) cpuCycles.getCycles() / ( double ) ( loops * size ) << " sum = " << sum <<  std::endl;

}
