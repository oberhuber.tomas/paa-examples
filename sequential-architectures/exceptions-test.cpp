/***************************************************************************
                          exceptions-test.cpp  -  description
                             -------------------
    begin                : 2015/02/04
    copyright            : (C) 2015 by Tomáš Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <TNL/Benchmarks/Benchmarks.h>
#include <TNL/Config/ConfigDescription.h>
#include <TNL/Config/parseCommandLine.h>

using namespace std;

void performTest( const int* data,
                  const int size,
                  int& a )
{
   a = 0;
   for( int i = 0; i < size; i++ )
   {
      if( data[ i ] == 1 )
         a++;
      if( data[ i ] == 0 )
         a = 0;
   }
}

void performTestWithException( const int* data,
                               const int size,
                               int& a )
{
   a = 0;
   for( int i = 0; i < size; i++ )
   {
      if( data[ i ] == 1 )
         a++;
      if( data[ i ] == 0 )
         throw -1;
   }
}

void configSetup( TNL::Config::ConfigDescription& config )
{
   config.addDelimiter("Benchmark settings:");
   config.addEntry<TNL::String>("log-file", "Log file name.", "exceptions.log");
   config.addEntry<TNL::String>("output-mode", "Mode for opening the log file.", "overwrite");
   config.addEntryEnum("overwrite");
   config.addEntryEnum("append");
   config.addEntry< bool >( "verbose", "Verbose mode.", true );
}

int main( int argc, char* argv[] )
{
   const int size = 256;
   const int loops = 2097152;

   TNL::Config::ConfigDescription config;
   configSetup( config );
   TNL::Config::ParameterContainer parameters;
   if( !TNL::Config::parseCommandLine( argc, argv, config, parameters ) )
      return EXIT_FAILURE;

   const auto verbose = parameters.getParameter< bool >( "verbose" );
   const auto log_file_name = parameters.getParameter< TNL::String >( "log-file" );
   const auto output_mode = parameters.getParameter< TNL::String >( "output-mode" );
   auto mode = std::ios::out;
   if( output_mode == "append" )
      mode |= std::ios::app;
   std::ofstream log_file( log_file_name.getString(), mode );
   TNL::Benchmarks::Benchmark<> benchmark(log_file, loops, verbose);
   benchmark.setOperationsPerLoop( size );

   // write global metadata into a separate file
   std::map< std::string, std::string > metadata = TNL::Benchmarks::getHardwareMetadata();
   TNL::Benchmarks::writeMapAsJson( metadata, log_file_name, ".metadata.json" );

   int* data = new int[ size ];
   for( int i = 0; i < size; i++ )
      data[ i ] = 1;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "test", "without exceptions", },
      { "throw", "no" }
   }));
   int a = 0;
   auto f1 = [&] () mutable { performTest( data, size, a ); };
   benchmark.time< TNL::Devices::Host >( "host", f1 );

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "test", "with exceptions", },
      { "throw", "no" }
   }));
   auto f2 = [&] () mutable {
      try
      {
         performTestWithException( data, size, a );
      }
      catch( int e )
      {
         std::cerr << "Exception" << e << std::endl;
      }
   };
   benchmark.time< TNL::Devices::Host >( "host", f2 );

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "test", "with exceptions", },
      { "throw", "yes" }
   }));
   data[ 0 ] = 0;
   int b = 0;
   auto f3 =  [&] () mutable {
      try
      {
         performTestWithException( data, size, a );
      }
      catch( int e )
      {
         b += e;
      }
   };
   benchmark.time< TNL::Devices::Host >( "host", f3 );
   std::cerr << b << std::endl;
   delete[] data;
   return EXIT_SUCCESS;
}
