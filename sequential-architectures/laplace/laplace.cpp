/***************************************************************************
                          laplace.cpp  -  description
                             -------------------
    begin                : 2015/02/04
    copyright            : (C) 2015 by Tomáš Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cstdlib>
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <TimerRT.h>
#include <PapiCounter.h>

using namespace std;

template< typename Real >
class Array
{
   public:
      Array( int size )
      {
         this->data = new Real[ size * size ];
         this->size = size;
      }

      void setValue( const Real& v )
      {
         for( int i = 0; i < size * size; i++ )
            this->data[ i ] = v;
      }

      void computeLaplace( const Array< Real >& a )
      {
         const double& h = 1.0 / ( double ) size;
         const double& h2 = 1.0 / ( h * h );
         for( int i = 1; i < size - 1; i++ )
         {
            int c = i * size + 1;
            int n = c + size;
            int s = c - size;
            int e = c + 1;
            int w = c - 1;
            for( int j = 1; j < size - 1; j++ )
            {
               /*if( i == 0 || j == 0 || i == size - 1 || j == size - 1 )
               {
                  this-> data[ i * size + j ] = 0.0;
                  continue;
               }*/
               this->data[ c ] = h2 *
                  ( ( a.data[ c + size ] +
                    a.data[ c - size ] ) +
                    ( a.data[ c + 1 ] +
                    a.data[ c - 1 ] ) -
                    4.0 * a.data[ c++ ] );
            }
         }
      }

      template< int blockSize = 128 >
      void computeLaplaceBlockwise( const Array< Real >& a )
      {
         const double& h = 1.0 / ( double ) size;
         const double& h2 = 1.0 / ( h * h );
         for( int i = 1; i < size - 1; i += blockSize )
            for( int j = 1; j < size - 1; j += blockSize )
            {
               const int stopI = min( i + blockSize, size - 1 );
               const int stopJ = min( j + blockSize, size - 1 );
               int c = i * size + j;
               /*int n = c + size;
               int s = c - size;
               int e = c + 1;
               int w = c - 1;*/
               for( int i1 = i; i1 < stopI; i1++ )
                  for( int j1 = j; j1 < stopJ; j1++ )
                  {
                     /*if( i1 == 0 || j1 == 0 || i1 == size -1 || j1 == size - 1 )
                     {
                        this-> data[ i1 * size + j1 ] = 0.0;
                        continue;
                     }*/
                     this->data[ c ] = h2 *
                        ( ( a.data[ c + size ] +
                          a.data[ c - size ] ) +
                          ( a.data[ c + 1 ] +
                          a.data[ c - 1 ] ) -
                          ( 4.0 * a.data[ c++ ] ) );
                  }
            }
      }


      ~Array()
      {
         delete[] this->data;
      }
   protected:
      Real* data;

      int size;
};

void writeTableHeader( ostream& str )
{
   str << std::right;
   str << "#";
   str << std::setw( 135 ) << std::setfill( '-') << "-" << std::endl;
   str << "#";
   str << std::setfill( ' ' );
   str << std::setw( 30 ) << "Array size         "
       << std::setw( 45 ) << "Common laplace       "
       << std::setw( 45 ) << "Block laplace       "
       << std::setw( 15 ) << "Speed-up"
       << std::endl;
   str << "#";
   str << std::setw( 15 ) << "size"
       << std::setw( 15 ) << "log2"
       << std::setw( 15 ) << "s"
       << std::setw( 15 ) << "GFLOPS"
       << std::setw( 15 ) << "L1 cache eff."
       << std::setw( 15 ) << "s"
       << std::setw( 15 ) << "GFLOPS"
       << std::setw( 15 ) << "L1 cache eff."
       << std::endl;
   str << "#";
   str << std::setw( 135 ) << std::setfill( '-') << "-" << std::endl;
   str << std::setfill( ' ' );
}

void writeTableLine( ostream& str,
                     const int size,
                     const double& baselineLaplaceTime,
                     const double& baselineLaplaceGflops,
                     const double& baselineLaplaceCacheEfficiency,
                     const double& blockLaplaceTime,
                     const double& blockLaplaceGflops,
                     const double& blockLaplaceCacheEfficiency )
{
   str << std::setw( 15 ) << size
       << std::setw( 15 ) << log2( size )
       << std::setw( 15 ) << baselineLaplaceTime
       << std::setw( 15 ) << baselineLaplaceGflops
       << std::setw( 15 ) << baselineLaplaceCacheEfficiency
       << std::setw( 15 ) << blockLaplaceTime
       << std::setw( 15 ) << blockLaplaceGflops
       << std::setw( 15 ) << blockLaplaceCacheEfficiency
       << std::setw( 15 ) << baselineLaplaceTime / blockLaplaceTime
       << std::endl;
}

int main( int argc, char* argv[] )
{
   const long int maxSize = 16384;
   const double minTestTime( 1.0 );
   TimerRT timer;

   std::fstream outputFile;
   outputFile.open( "results.txt", ios::out );
   if( ! outputFile )
   {
      std::cerr << "I cannot open the output file results.txt." << std::endl;
      return EXIT_FAILURE;
   }

   writeTableHeader( std::cout );
   writeTableHeader( outputFile );
   int size = 17;
   while( size < maxSize )
   {
      const long int operations = 6 * size * size;
      Array< float > array1( size ), array2( size );
      array2.setValue( 1.0 );
      int counter;
      double baselineCacheEfficiency, blockwiseCacheEfficiency;

      counter = 0;
      timer.reset();
      timer.start();
      PapiCounter papiCounter;
#ifdef HAVE_PAPI
      papiCounter.setNumberOfEvents( 2 );
      papiCounter.addEvent( PAPI_L1_DCA );
      papiCounter.addEvent( PAPI_L1_DCH );
#endif
      papiCounter.reset();
      papiCounter.start();

      while( timer.getTime() < minTestTime )
      {
         array1.computeLaplace( array2 );
         counter++;
      }
      timer.stop();
#ifdef HAVE_PAPI
      baselineCacheEfficiency = ( double ) papiCounter.getEventValue( PAPI_L1_DCH ) / ( double ) papiCounter.getEventValue( PAPI_L1_DCA );
#endif
      double baselineTime = timer.getTime() / ( double ) counter;
      double baselineGflops = operations / baselineTime / 1.0e9;

      counter = 0;
      timer.reset();
      timer.start();
      papiCounter.reset();
      papiCounter.start();

      while( timer.getTime() < minTestTime )
      {
         array1.computeLaplaceBlockwise( array2 );
         counter++;
      }
      timer.stop();
#ifdef HAVE_PAPI
      blockwiseCacheEfficiency = ( double ) papiCounter.getEventValue( PAPI_L1_DCH ) / ( double ) papiCounter.getEventValue( PAPI_L1_DCA );
#endif
      double blockTime = timer.getTime() / ( double ) counter;
      double blockGflops = operations / blockTime / 1.0e9;

      writeTableLine( std::cout,
                      size,
                      baselineTime,
                      baselineGflops,
                      baselineCacheEfficiency,
                      blockTime,
                      blockGflops,
                      blockwiseCacheEfficiency );
      writeTableLine( outputFile,
                      size,
                      baselineTime,
                      baselineGflops,
                      baselineCacheEfficiency,
                      blockTime,
                      blockGflops,
                      blockwiseCacheEfficiency );
      size = 2 *  size - 1;
   }
}


