/***************************************************************************
                          MatrixAdditon.cpp  -  description
                             -------------------
    begin                : 2015/02/04
    copyright            : (C) 2015 by Tomáš Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cstdlib>
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <TNL/Benchmarks/Benchmarks.h>
#include <TNL/Config/ConfigDescription.h>
#include <TNL/Config/parseCommandLine.h>

template< typename Real >
class StdMatrix
{
   public:
      StdMatrix( int size )
      {
         this->matrix.resize( size );
         for( int i = 0; i < size; i++ )
            this->matrix[ i ].resize( size );
         this->size = size;
      };

      void setValue( const Real& v )
      {
         for( int i = 0; i < size; i++ )
            for( int j = 0; j < size; j++ )
               this->matrix[ i ][ j ] = v;
      }

      void addMatrixRowWise( const StdMatrix< Real >& m )
      {
         for( int i = 0; i < size; i++ )
            for( int j = 0; j < size; j++ )
               this->matrix[ i ][ j ] += m.matrix[ i ][ j ];
      }

      void addMatrixColumnWise( const StdMatrix< Real >& m )
      {
         for( int j = 0; j < size; j++ )
            for( int i = 0; i < size; i++ )
               this->matrix[ i ][ j ] += m.matrix[ i ][ j ];
      }


   protected:
      std::vector< std::vector< Real > > matrix;

      int size;
};

template< typename Real >
class ArrayMatrix
{
   public:
      ArrayMatrix( int size )
      {
         this->matrix = new Real[ size * size ];
         this->size = size;
      }

      void setValue( const Real& v )
      {
         for( int i = 0; i < size * size; i++ )
            this->matrix[ i ] = v;
      }

      void addMatrixRowWise( const ArrayMatrix< Real >& m )
      {
         for( int i = 0; i < size; i++ )
            for( int j = 0; j < size; j++ )
               this->matrix[ i * size + j ] += m.matrix[ i * size + j ];
      }

      void addMatrixColumnWise( const ArrayMatrix< Real >& m )
      {
         for( int j = 0; j < size; j++ )
            for( int i = 0; i < size; i++ )
               this->matrix[ i * size + j ] += m.matrix[ i * size + j ];
      }


      ~ArrayMatrix()
      {
         delete[] this->matrix;
      }
   protected:
      Real* matrix;

      int size;
};

void configSetup( TNL::Config::ConfigDescription& config )
{
   config.addDelimiter("Benchmark settings:");
   config.addEntry<TNL::String>("log-file", "Log file name.", "matrix-addition.log");
   config.addEntry<TNL::String>("output-mode", "Mode for opening the log file.", "overwrite");
   config.addEntryEnum("overwrite");
   config.addEntryEnum("append");
   config.addEntry< int >( "min-matrix-size", "Minimal array size size.", 16  );
   config.addEntry< int >( "max-matrix-size", "Maximal array size size.", 16384 );
   config.addEntry< bool >( "verbose", "Verbose mode.", true );
}

int main( int argc, char* argv[] )
{
   TNL::Config::ConfigDescription config;
   configSetup( config );
   TNL::Config::ParameterContainer parameters;
   if( !TNL::Config::parseCommandLine( argc, argv, config, parameters ) )
      return EXIT_FAILURE;

   const auto minSize = parameters.getParameter< int >( "min-matrix-size" );
   const auto maxSize = parameters.getParameter< int >( "max-matrix-size" );
   const auto verbose = parameters.getParameter< bool >( "verbose" );
   const auto log_file_name = parameters.getParameter< TNL::String >( "log-file" );
   const auto output_mode = parameters.getParameter< TNL::String >( "output-mode" );
   auto mode = std::ios::out;
   if( output_mode == "append" )
      mode |= std::ios::app;
   std::ofstream log_file( log_file_name.getString(), mode );
   TNL::Benchmarks::Benchmark<> benchmark(log_file, 1, verbose);

   // write global metadata into a separate file
   std::map< std::string, std::string > metadata = TNL::Benchmarks::getHardwareMetadata();
   TNL::Benchmarks::writeMapAsJson( metadata, log_file_name, ".metadata.json" );

   for( size_t size = minSize; size <= maxSize; size *= 2 )
   {
      const long int elements = size * size;
      const int repeating = maxSize * maxSize / elements;
      StdMatrix< float > stdMatrix1( size ), stdMatrix2( size );
      ArrayMatrix< float > arrayMatrix1( size ), arrayMatrix2( size );
      stdMatrix1.setValue( 0.0 );
      stdMatrix2.setValue( 1.0 );
      arrayMatrix1.setValue( 0.0 );
      arrayMatrix2.setValue( 1.0 );

      benchmark.setLoops( 2 * (maxSize/size)*(maxSize/size) );
      benchmark.setOperationsPerLoop( size * size );
      benchmark.setDatasetSize( ( double ) ( 2 * size * size * sizeof( float ) ) / ( double ) ( 1 << 30 ) );

      benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
         { "size", TNL::convertToString( size ), },
         { "implementation", "STL" },
         { "ordering", "row-wise" },
      }));
      auto f1 = [&] () { stdMatrix1.addMatrixRowWise( stdMatrix2 );};
      benchmark.time< TNL::Devices::Host >( "host", f1 );

      benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
         { "size", TNL::convertToString( size ), },
         { "implementation", "STL" },
         { "ordering", "column-wise" },
      }));
      auto f2 = [&] () { stdMatrix1.addMatrixColumnWise( stdMatrix2 );};
      benchmark.time< TNL::Devices::Host >( "host", f2 );

      benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
         { "size", TNL::convertToString( size ), },
         { "implementation", "array" },
         { "ordering", "row-wise" },
      }));
      auto f3 = [&] () { arrayMatrix1.addMatrixRowWise( arrayMatrix2 );};
      benchmark.time< TNL::Devices::Host >( "host", f3 );

      benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
         { "size", TNL::convertToString( size ), },
         { "implementation", "array" },
         { "ordering", "column-wise" },
      }));
      auto f4 = [&] () { arrayMatrix1.addMatrixColumnWise( arrayMatrix2 );};
      benchmark.time< TNL::Devices::Host >( "host", f4 );
   }
}

