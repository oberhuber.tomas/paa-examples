/***************************************************************************
                          MatrixAdditon.cpp  -  description
                             -------------------
    begin                : 2015/02/04
    copyright            : (C) 2015 by Tomáš Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cstdlib>
#include <cstring>
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <TNL/Benchmarks/Benchmarks.h>
#include <TNL/Config/ConfigDescription.h>
#include <TNL/Config/parseCommandLine.h>

using namespace std;

template< typename Real >
class ArrayMatrix
{
   public:
      ArrayMatrix( int size )
      {
         this->matrix = new Real[ size * size ];
         this->size = size;
      }

      void setValue( const Real& v )
      {
         for( int i = 0; i < size * size; i++ )
            this->matrix[ i ] = v;
      }

      void setElement( int i, int j, const Real& v )
      {
         this->matrix[ i * size + j ] = v;
      }

      ArrayMatrix& operator = ( const ArrayMatrix& m )
      {
         memcpy( this->matrix, m.matrix, size * size * sizeof( Real ) );
         return ( *this );
      }

      bool operator == ( const ArrayMatrix& m ) const
      {
         if( memcmp( this->matrix, m.matrix, size * size * sizeof( Real ) ) == 0 )
            return true;
         return false;
      }

      bool operator != ( const ArrayMatrix& m ) const
      {
         return ! this->operator == ( m );
      }

      void multiplyMatrices( const ArrayMatrix< Real >& m1,
                             const ArrayMatrix< Real >& m2 )
      {
         for( int i = 0; i < size; i++ )
            for( int j = 0; j < size; j++ )
            {
               Real aux( 0.0 );
               for( int k = 0; k < size; k++ )
                  aux += m1.matrix[ i * size + k ] * m2.matrix[ k * size + j ];
               this->matrix[ i * size + j ] = aux;
            }
      }

      void multiplyMatricesWithTransposition( const ArrayMatrix< Real >& m1,
                                              const ArrayMatrix< Real >& m2 )
      {
         Real* transposedMatrix = new Real[ size * size ];
         for( int i = 0; i < size; i++ )
            for( int j = 0; j < size; j++ )
               transposedMatrix[ i * size + j] =
                  m2.matrix[ j * size + i ];

         for( int i = 0; i < size; i++ )
            for( int j = 0; j < size; j++ )
            {
               Real aux( 0.0 );
               for( int k = 0; k < size; k++ )
                  aux += m1.matrix[ i * size + k ] * transposedMatrix[ j * size + k ];
               this->matrix[ i * size + j ] = aux;
            }

         delete[] transposedMatrix;
      }

      template< int blockSize = 16 >
      void multiplyMatricesBlockwise1( const ArrayMatrix< Real >& m1,
                                       const ArrayMatrix< Real >& m2 )
      {
         memset( this->matrix, 0, size * size * sizeof( Real  ) );
         for( int i = 0; i < size; i += blockSize )
            for( int j = 0; j < size; j += blockSize )
               for( int k = 0; k < size; k += blockSize )
               {
                  for( int i1 = i; i1 < i + blockSize; i1++ )
                     for( int j1 = j; j1 < j + blockSize; j1++ )
                        for( int k1 = k; k1 < k + blockSize; k1++ ) {
                            this->matrix[ i1*size + j1 ] +=
                              m1.matrix[ i1*size + k1 ]*
                                 m2.matrix[ k1*size  + j1 ];
                        }

               }
      }
      template< int blockSize = 16 >
      void multiplyMatricesBlockwise2( const ArrayMatrix< Real >& m1,
                                       const ArrayMatrix< Real >& m2 )
      {
         memset( this->matrix, 0, size * size * sizeof( Real  ) );
         for( int i = 0; i < size; i += blockSize )
            for( int j = 0; j < size; j += blockSize )
               for( int k = 0; k < size; k += blockSize )
               {
                  Real* res = &this->matrix[ i * size + j ];
                  Real* _m1 = &m1.matrix[ i*size + k ];
                  for( int i1 = 0; i1 < blockSize; i1++, res += size, _m1 += size )
                     for( int j1 = 0; j1 < blockSize; j1++ )
                     {
                        Real* _m2 = &m2.matrix[ k*size + j ];
                        for( int k1 = 0; k1 < blockSize; k1++, _m2 += size )
                           res[ j1 ] += _m1[ k1 ] * _m2[ j1 ];
                     }
               }
      }

      template< int blockSize = 16 >
      void multiplyMatricesBlockwise3( const ArrayMatrix< Real >& m1,
                                       const ArrayMatrix< Real >& m2 )
      {
         memset( this->matrix, 0, size * size * sizeof( Real  ) );
         for( int i = 0; i < size; i += blockSize )
           for( int j = 0; j < size; j += blockSize )
              for( int k = 0; k < size; k += blockSize )
              {
                 Real* res = &this->matrix[ i * size + j ];
                 Real* _m1 = &m1.matrix[ i*size + k ];
                 for( int i1 = 0; i1 < blockSize; i1++, res += size, _m1 += size )
                 {
                    Real* _m2 = &m2.matrix[ k*size + j ];
                    for( int k1 = 0; k1 < blockSize; k1++, _m2 += size )
                       for( int j1 = 0; j1 < blockSize; j1++ )
                          res[ j1 ] += _m1[ k1 ] * _m2[ j1 ];
                 }
              }
      }

      bool check( const ArrayMatrix< Real >& m, Real tolerance = 1.0e-6 )
      {
         for( int i = 0; i < size; i++ )
            for( int j = 0; j < size; j++ )
               if( this->matrix[ i * size + j ] != m.matrix[ i * size + j ] )
               {
                  std::cerr << "Error of position: " << i << " " << j << " -> "
                            << this->matrix[ i * size + j ] << " x " << m.matrix[ i * size + j ] << std::endl;
                  std::cerr << "this = " << std::endl << *this << std::endl << "m = " << std::endl << m << std::endl;
                  return false;
               }
         return true;
      }

      void print( ostream& str ) const
      {
         str << "Ptr = " << matrix << std::endl;
         for( int i = 0; i < size; i++ )
         {
            for( int j = 0; j < size; j++ )
               str << matrix[ i * size + j ] << "\t";
            str << std::endl;
         }
      }

      ~ArrayMatrix()
      {
         delete[] this->matrix;
      }
   protected:
      Real* matrix = nullptr;

      int size;
};

template< typename Real >
std::ostream& operator<<( std::ostream& str, const ArrayMatrix< Real >& m )
{
   m.print( str );
   return str;
}

void configSetup( TNL::Config::ConfigDescription& config )
{
   config.addDelimiter("Benchmark settings:");
   config.addEntry<TNL::String>("log-file", "Log file name.", "matrix-multiplication.log");
   config.addEntry<TNL::String>("output-mode", "Mode for opening the log file.", "overwrite");
   config.addEntryEnum("overwrite");
   config.addEntryEnum("append");
   config.addEntry< int >( "min-matrix-size", "Minimal array size size.", 16  );
   config.addEntry< int >( "max-matrix-size", "Maximal array size size.", 8192 );
   config.addEntry< bool >( "verbose", "Verbose mode.", true );
}

int main( int argc, char* argv[] )
{
   TNL::Config::ConfigDescription config;
   configSetup( config );
   TNL::Config::ParameterContainer parameters;
   if( !TNL::Config::parseCommandLine( argc, argv, config, parameters ) )
      return EXIT_FAILURE;

   const auto minSize = parameters.getParameter< int >( "min-matrix-size" );
   const auto maxSize = parameters.getParameter< int >( "max-matrix-size" );
   const auto verbose = parameters.getParameter< bool >( "verbose" );
   const auto log_file_name = parameters.getParameter< TNL::String >( "log-file" );
   const auto output_mode = parameters.getParameter< TNL::String >( "output-mode" );
   auto mode = std::ios::out;
   if( output_mode == "append" )
      mode |= std::ios::app;
   std::ofstream log_file( log_file_name.getString(), mode );
   TNL::Benchmarks::Benchmark<> benchmark(log_file, 1, verbose);

   // write global metadata into a separate file
   std::map< std::string, std::string > metadata = TNL::Benchmarks::getHardwareMetadata();
   TNL::Benchmarks::writeMapAsJson( metadata, log_file_name, ".metadata.json" );

   for( size_t size = minSize; size <= maxSize; size *= 2 )
   {
      if( size % 16 != 0 )
      {
         std::cerr << "ERROR: Matrix size ( " << size << " ) must be multiple of 16." << std::endl;
         continue;
      }
      const double operations = 2 * size * size * size;
      ArrayMatrix< float > a( size ), b( size ), c( size ), d( size );
      a.setValue( 1.0 );
      b.setValue( 0.0 );
      for( int i = 0; i < size; i++ )
         b.setElement( i, i, i + 1 );

      benchmark.setLoops( 2 * (maxSize/size) );
      benchmark.setOperationsPerLoop( size * size * size );
      double datasetSize = ( double ) ( 2 * size * size * size * sizeof( float ) ) / ( double ) ( 1 << 30 );

      benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
         { "size", TNL::convertToString( size ), }
      }));
      benchmark.setOperation( "baseline", datasetSize );
      auto f1 = [&] () mutable { c.multiplyMatrices( a, b ); };
      benchmark.time< TNL::Devices::Sequential >( "host", f1 );
      d = c;

      benchmark.setOperation( "transposition", datasetSize );
      auto f2 = [&] () mutable { c.multiplyMatricesWithTransposition( a, b ); };
      benchmark.time< TNL::Devices::Host >( "host", f2 );
      if( !c.check( d ) )
      {
         std::cerr << "Multiplication with transposition gave wrong result." << std::endl;
         return EXIT_FAILURE;
      }

      benchmark.setOperation( "blocks 1", datasetSize );
      auto f3 = [&] () mutable { c.template multiplyMatricesBlockwise1< 16 >( a, b ); };
      benchmark.time< TNL::Devices::Host >( "host", f3 );
      if( !c.check( d ) )
      {
         std::cerr << "Multiplication with block version 1 gave wrong result." << std::endl;
         return EXIT_FAILURE;
      }

      benchmark.setOperation( "blocks 2", datasetSize );
      auto f4 = [&] () mutable { c.template multiplyMatricesBlockwise2< 16 >( a, b ); };
      benchmark.time< TNL::Devices::Host >( "host", f4 );
      if( !c.check( d ) )
      {
         std::cerr << "Multiplication with block version 2 gave wrong result." << std::endl;
         return EXIT_FAILURE;
      }

      benchmark.setOperation( "blocks 3", datasetSize );
      auto f5 = [&] () mutable { c.template multiplyMatricesBlockwise3< 16 >( a, b ); };
      benchmark.time< TNL::Devices::Host >( "host", f5 );
      if( d != c )
      {
         std::cerr << "Multiplication with block version 3 gave wrong result." << std::endl;
         return EXIT_FAILURE;
      }
   }
}

