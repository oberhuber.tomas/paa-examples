/***************************************************************************
                          MatrixTransposition.cpp  -  description
                             -------------------
    begin                : 2015/02/04
    copyright            : (C) 2015 by Tomáš Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cstdlib>
#include <cstring>
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <TNL/Benchmarks/Benchmarks.h>
#include <TNL/Config/ConfigDescription.h>
#include <TNL/Config/parseCommandLine.h>

using namespace std;

template< typename Real >
class ArrayMatrix
{
   public:
      ArrayMatrix( int size )
      {
         this->matrix = new Real[ size * size ];
         //posix_memalign( ( void** ) &this->matrix, 64, size * size * sizeof( Real ) );
         this->size = size;
      }

      void setValue( const Real& v )
      {
         for( int i = 0; i < size * size; i++ )
            this->matrix[ i ] = v;
      }

      void setElement( int i, int j, const Real& v )
      {
         this->matrix[ i * size + j ] = v;
      }

      void transpose()
      {
         Real tmp;
         for( int i = 1; i < size; i++ )
            for( int j = 0; j < i; j++ )
            {
               tmp = this->matrix[ i * size + j ];
               this->matrix[ i * size + j ] = this->matrix[ j * size + i ];
               this->matrix[ j * size + i ] = tmp;
            }
      }

      ~ArrayMatrix()
      {
         delete[] this->matrix;
      }
   protected:
      Real* matrix;

      int size;
};

void configSetup( TNL::Config::ConfigDescription& config )
{
   config.addDelimiter("Benchmark settings:");
   config.addEntry<TNL::String>("log-file", "Log file name.", "matrix-transposition.log");
   config.addEntry<TNL::String>("output-mode", "Mode for opening the log file.", "overwrite");
   config.addEntryEnum("overwrite");
   config.addEntryEnum("append");
   config.addEntry< int >( "min-matrix-size", "Minimal array size size.", 8  );
   config.addEntry< int >( "max-matrix-size", "Maximal array size size.", 2048 );
   config.addEntry< bool >( "verbose", "Verbose mode.", true );
}

int main( int argc, char* argv[] )
{
   TNL::Config::ConfigDescription config;
   configSetup( config );
   TNL::Config::ParameterContainer parameters;
   if( !TNL::Config::parseCommandLine( argc, argv, config, parameters ) )
      return EXIT_FAILURE;

   const auto minSize = parameters.getParameter< int >( "min-matrix-size" );
   const auto maxSize = parameters.getParameter< int >( "max-matrix-size" );
   const auto verbose = parameters.getParameter< bool >( "verbose" );
   const auto log_file_name = parameters.getParameter< TNL::String >( "log-file" );
   const auto output_mode = parameters.getParameter< TNL::String >( "output-mode" );
   auto mode = std::ios::out;
   if( output_mode == "append" )
      mode |= std::ios::app;
   std::ofstream log_file( log_file_name.getString(), mode );
   TNL::Benchmarks::Benchmark<> benchmark(log_file, 1, verbose);

   // write global metadata into a separate file
   std::map< std::string, std::string > metadata = TNL::Benchmarks::getHardwareMetadata();
   TNL::Benchmarks::writeMapAsJson( metadata, log_file_name, ".metadata.json" );

   for( size_t sizeBase = minSize; sizeBase <= maxSize; sizeBase *= 2 )
      for( size_t size = sizeBase - 1; size <= sizeBase + 1; size++ )
      {
         benchmark.setLoops( max( ( size_t ) 2, 2 * (maxSize/size)*(maxSize/size) ) );
         benchmark.setOperationsPerLoop( 0.5 * size * size );
         double datasetSize = ( double ) ( size * size * sizeof( float ) ) / ( double ) ( 1 << 30 );
         benchmark.setDatasetSize( datasetSize );

         ArrayMatrix< double > a( size );
         a.setValue( 1.0 );

         benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
            { "size", TNL::convertToString( size ), }
         }));
         auto f = [&] () mutable { a.transpose(); };
         benchmark.time< TNL::Devices::Host >( "host", f );
      }
   return EXIT_SUCCESS;
}
