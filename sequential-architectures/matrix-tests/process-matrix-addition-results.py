#!/usr/bin/python3

import os
import json
import pandas as pd
from pandas.io.json import json_normalize
import matplotlib.pyplot as plt
import numpy as np
import math
from os.path import exists

implementations = [ 'STL', 'array']
orderings = [ 'row-wise', 'column-wise' ]
sizes = []

####
# Create multiindex for columns
def get_multiindex():
    level1 = [ 'size' ]
    level2 = [ '' ]
    level3 = [ '' ]
    df_data = [[ ' ' ]]
    for implementation in implementations:
        for ordering in orderings:
            values = ['time', "bandwidth", "CPU cycles"]
            for value in values:
               level1.append( implementation )
               level2.append( ordering )
               level3.append( value )
               df_data[0].append( '' )

    multiColumns = pd.MultiIndex.from_arrays([ level1, level2, level3 ] )
    return multiColumns, df_data

def processDf( df ):
   multicolumns, df_data = get_multiindex()

   frames = []
   in_idx = 0
   out_idx = 0

   for size in sizes:
      aux_df=df.loc[ ( df['size'] == float( size ) ) ]
      new_df = pd.DataFrame( df_data, columns = multicolumns, index = [out_idx] )
      new_df.iloc[0][ ('size','','') ] = int( size )
      for index, row in aux_df.iterrows():
         implementation = row[ 'implementation' ]
         ordering = row[ 'ordering' ]
         time = row[ 'time' ]
         bandwidth = row[ 'bandwidth' ]
         cpu_cycles = row[ 'cycles/op.' ]
         new_df.iloc[0][( implementation, ordering, 'time') ] = float( time )
         new_df.iloc[0][( implementation, ordering, 'bandwidth') ] = float( bandwidth )
         try:
            new_df.iloc[0][( implementation, ordering, 'CPU cycles') ] = float( cpu_cycles )
         except ValueError:
            new_df.iloc[0][( implementation, ordering, 'CPU cycles') ] = 0
      frames.append( new_df)
      out_idx = out_idx+1
   result = pd.concat( frames )
   print( result )
   return result

def writeImages( df ):
   sizes = df[('size','','')].tolist()
   fig, axs = plt.subplots( 1, 1 )
   legend = []
   for implementation in implementations:
      for ordering in orderings:
         bandwidth  = df[(implementation, ordering,'bandwidth')].tolist()
         axs.plot( sizes, bandwidth, '-o', ms=4, lw=1 )
         legend.append( f'{implementation} {ordering}' )
   axs.legend( legend, loc='upper right' )
   axs.set_ylabel( 'Effective bandwidth in GB/sec' )
   axs.set_xlabel( 'Array size in bytes' )
   axs.set_xscale( 'log' )
   axs.set_yscale( 'linear' )
   axs.grid()
   #axs.set_ylim( [0, 1.2*max_bandwidth ])
   plt.rcParams.update({
   "text.usetex": True,
   "font.family": "sans-serif"})
   plt.savefig( f"matrix-addition.pdf")
   plt.close(fig)


#####
# Parse input files
parsed_lines = []
filename = f"matrix-addition.log"
if not exists( filename ):
    print( f"Skipping non-existing input file {filename} ...." )
print( f"Parsing input file {filename} ...." )
with open( filename ) as f:
    lines = f.readlines()
    for line in lines:
        parsed_line = json.loads(line)
        parsed_lines.append( parsed_line )

df = pd.DataFrame(parsed_lines)

for size in df['size']:
    if size not in sizes:
        sizes.append( size )

keys = ['size','time', 'bandwidth', 'cycles/op' ]

for key in keys:
    if key in df.keys():
        df[key] = pd.to_numeric(df[key])

df.to_html( 'matrix-addition-raw.html', index=False )
frame = processDf( df )
frame.to_html( 'matrix-addition.html', index=False )
writeImages( frame )
