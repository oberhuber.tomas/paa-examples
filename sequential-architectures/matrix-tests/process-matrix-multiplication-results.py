#!/usr/bin/python3

import os
import json
import pandas as pd
from pandas.io.json import json_normalize
import matplotlib.pyplot as plt
import numpy as np
import math
from os.path import exists

implementations = [ 'baseline', 'transposition', 'blocks 1', 'blocks 2', 'blocks 3']
sizes = []

####
# Create multiindex for columns
def get_multiindex():
    level1 = [ 'size' ]
    level2 = [ '' ]
    df_data = [[ '' ]]
    for implementation in implementations:
        values = ['time', "bandwidth", "CPU cycles"]
        for value in values:
            level1.append( implementation )
            level2.append( value )
            df_data[0].append( '' )
    level1.append( 'speed-up' )
    level2.append( '' )
    df_data[0].append( '' )
    multiColumns = pd.MultiIndex.from_arrays([ level1, level2 ] )
    return multiColumns, df_data

def processDf( df ):
   multicolumns, df_data = get_multiindex()

   frames = []
   out_idx = 0

   for size in sizes:
      aux_df=df.loc[ ( df['size'] == float( size ) ) ]
      new_df = pd.DataFrame( df_data, columns = multicolumns, index = [out_idx] )
      new_df.iloc[0][ ('size','') ] = int( size )
      baseline_bw = 0
      blocks_3_bw = 0
      for index, row in aux_df.iterrows():
         implementation = row[ 'operation' ]
         time = row[ 'time' ]
         bandwidth = row[ 'bandwidth' ]
         if implementation == 'baseline':
            baseline_bw = float( bandwidth )
         if implementation == 'blocks 3':
            blocks_3_bw = float( bandwidth )
         cpu_cycles = row[ 'cycles/op.' ]
         new_df.iloc[0][( implementation, 'time') ] = float( time )
         new_df.iloc[0][( implementation, 'bandwidth') ] = float( bandwidth )
         try:
            new_df.iloc[0][( implementation, 'CPU cycles') ] = float( cpu_cycles )
         except ValueError:
            new_df.iloc[0][( implementation, 'CPU cycles') ] = 0
         new_df.iloc[0][( 'speed-up','')] = blocks_3_bw / baseline_bw
      frames.append( new_df)
      out_idx = out_idx+1
   result = pd.concat( frames )
   return result

def writeImages( df, current_implementations, filename ):
   sizes = df[('size','')].tolist()
   fig, axs = plt.subplots( 1, 1 )
   legend = []
   for implementation in current_implementations:
       bandwidth  = df[(implementation, 'bandwidth')].tolist()
       axs.plot( sizes, bandwidth, '-o', ms=4, lw=1 )
       legend.append( f'{implementation}' )
   axs.legend( legend, loc='lower center' )
   axs.set_ylabel( 'Effective bandwidth in GB/sec' )
   axs.set_xlabel( 'Array size in bytes' )
   axs.set_xscale( 'log' )
   axs.set_yscale( 'linear' )
   axs.grid()
   #axs.set_ylim( [0, 1.2*max_bandwidth ])
   plt.rcParams.update({ "text.usetex": True, "font.family": "sans-serif"})
   plt.savefig( filename )
   plt.close(fig)

def writeSpeedup( df ):
   sizes = df[('size','')].tolist()
   fig, axs = plt.subplots( 1, 1 )
   speedup = df[('speed-up', '')].tolist()
   axs.plot( sizes, speedup, '-o', ms=4, lw=1 )
   axs.set_ylabel( 'Speed-up' )
   axs.set_xlabel( 'Array size in bytes' )
   axs.set_xscale( 'log' )
   axs.set_yscale( 'linear' )
   axs.grid()
   #axs.set_ylim( [0, 1.2*max_bandwidth ])
   plt.rcParams.update({ "text.usetex": True, "font.family": "sans-serif"})
   plt.savefig( 'matrix-multiplication-speed-up.pdf' )
   plt.close(fig)


#####
# Parse input files
parsed_lines = []
filename = f"matrix-multiplication.log"
if not exists( filename ):
    print( f"Skipping non-existing input file {filename} ...." )
print( f"Parsing input file {filename} ...." )
with open( filename ) as f:
    lines = f.readlines()
    for line in lines:
        parsed_line = json.loads(line)
        parsed_lines.append( parsed_line )

df = pd.DataFrame(parsed_lines)

for size in df['size']:
    if size not in sizes:
        sizes.append( size )

keys = ['size','time', 'bandwidth', 'cycles/op' ]

for key in keys:
    if key in df.keys():
        df[key] = pd.to_numeric(df[key])

df.to_html( 'matrix-multiplication-raw.html', index=False )
frame = processDf( df )
print( frame )
frame.to_html( 'matrix-multiplication.html', index=False )
writeImages( frame, [ 'baseline' ],                                                     'matrix-multiplication-1.pdf' )
writeImages( frame, [ 'baseline', 'transposition' ],                                    'matrix-multiplication-2.pdf' )
writeImages( frame, [ 'baseline', 'transposition', 'blocks 1' ],                        'matrix-multiplication-3.pdf' )
writeImages( frame, [ 'baseline', 'transposition', 'blocks 1', 'blocks 2' ],            'matrix-multiplication-4.pdf' )
writeImages( frame, [ 'baseline', 'transposition', 'blocks 1', 'blocks 2', 'blocks 3'], 'matrix-multiplication-5.pdf' )
writeSpeedup( frame )