#!/usr/bin/python3

import os
import json
import pandas as pd
from pandas.io.json import json_normalize
import matplotlib.pyplot as plt
import numpy as np
import math
from os.path import exists

sizes = []

####
# Create multiindex for columns
def get_multiindex():
    level1 =  [ 'size', 'time', 'bandwidth', 'CPU cycles' ]
    df_data = [[ '',    '',     '',          '' ]]

    multiColumns = pd.MultiIndex.from_arrays([ level1 ] )
    return multiColumns, df_data

####
# Process data frame
def processDf( df ):
   multicolumns, df_data = get_multiindex()

   frames = []
   out_idx = 0

   for size in sizes:
      aux_df=df.loc[ ( df['size'] == float( size ) ) ]
      new_df = pd.DataFrame( df_data, columns = multicolumns, index = [out_idx] )
      new_df.iloc[0][ ('size') ] = int( size )
      for index, row in aux_df.iterrows():
         time = row[ 'time' ]
         bandwidth = row[ 'bandwidth' ]
         cpu_cycles = row[ 'cycles/op.' ]
         new_df.iloc[0][( 'time') ] = float( time )
         new_df.iloc[0][( 'bandwidth') ] = float( bandwidth )
         try:
            new_df.iloc[0][( 'CPU cycles') ] = float( cpu_cycles )
         except ValueError:
            new_df.iloc[0][( 'CPU cycles') ] = 0
      frames.append( new_df)
      out_idx = out_idx+1
   result = pd.concat( frames )
   return result

####
# Write images
def writeImages( df ):
   sizes = df[('size')].values.tolist()
   fig, axs = plt.subplots( 1, 1 )
   bandwidth  = df[('bandwidth')].values.tolist()
   axs.plot( sizes, bandwidth, '-o', ms=4, lw=1 )
   axs.set_ylabel( 'Effective bandwidth in GB/sec' )
   axs.set_xlabel( 'Array size in bytes' )
   axs.set_xscale( 'log' )
   axs.set_yscale( 'linear' )
   axs.grid()
   plt.rcParams.update({
   "text.usetex": True,
   "font.family": "sans-serif"})
   plt.savefig( f"matrix-transposition.pdf")
   plt.close(fig)

#####
# Parse input files
parsed_lines = []
filename = f"matrix-transposition.log"
if not exists( filename ):
    print( f"Skipping non-existing input file {filename} ...." )
print( f"Parsing input file {filename} ...." )
with open( filename ) as f:
    lines = f.readlines()
    for line in lines:
        parsed_line = json.loads(line)
        parsed_lines.append( parsed_line )

df = pd.DataFrame(parsed_lines)

for size in df['size']:
    if size not in sizes:
        sizes.append( size )

keys = ['size','time', 'bandwidth', 'cycles/op' ]

for key in keys:
    if key in df.keys():
        df[key] = pd.to_numeric(df[key])

df.to_html( 'matrix-transposition-raw.html', index=False )
frame = processDf( df )
print( frame )
frame.to_html( 'matrix-transposition.html', index=False )
frame.to_latex( 'matrix-transposition.tex', index=False )
writeImages( frame )
