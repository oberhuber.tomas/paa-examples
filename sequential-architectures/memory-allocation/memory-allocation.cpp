/***************************************************************************
                          memory-allocation.cpp  -  description
                             -------------------
    begin                : Feb 23, 2015
    copyright            : (C) 2015 by Tomá¹ Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <alloca.h>
#include <stdlib.h>
#include <TNL/Benchmarks/Benchmarks.h>
#include <TNL/Config/ConfigDescription.h>
#include <TNL/Config/parseCommandLine.h>

using namespace std;

unsigned long long int  ctr = 0;

const int testSize = 1 << 16;
std::vector< int > sizes( testSize );

int a[ testSize ];

void globalArrayTest( int pos )
{
   ctr += a[ pos - 1 ];
}

void localArrayTest( int pos )
{
   int b[ testSize ];
   ctr += b[ pos - 1 ];
}

void stackTest( int pos )
{
   int* c = (int* )  alloca( pos * sizeof( int ) );
   ctr += c[ pos - 1 ];
}

void heapTest( int pos )
{
   int* c = (int* ) malloc( pos * sizeof( int ) );
   ctr += c[ pos - 1 ];
   free( ( void* ) c );
}

void configSetup( TNL::Config::ConfigDescription& config )
{
   config.addDelimiter("Benchmark settings:");
   config.addEntry<TNL::String>("log-file", "Log file name.", "memory-allocation.log");
   config.addEntry<TNL::String>("output-mode", "Mode for opening the log file.", "overwrite");
   config.addEntryEnum("overwrite");
   config.addEntryEnum("append");
   config.addEntry< bool >( "verbose", "Verbose mode.", true );
}

int main( int argc, char* argv[] )
{
   TNL::Config::ConfigDescription config;
   configSetup( config );
   TNL::Config::ParameterContainer parameters;
   if( !TNL::Config::parseCommandLine( argc, argv, config, parameters ) )
      return EXIT_FAILURE;

   const int loops = 1<<10;
   const auto verbose = parameters.getParameter< bool >( "verbose" );
   const auto log_file_name = parameters.getParameter< TNL::String >( "log-file" );
   const auto output_mode = parameters.getParameter< TNL::String >( "output-mode" );
   auto mode = std::ios::out;
   if( output_mode == "append" )
      mode |= std::ios::app;
   std::ofstream log_file( log_file_name.getString(), mode );
   TNL::Benchmarks::Benchmark<> benchmark(log_file, loops, verbose);
   benchmark.setOperationsPerLoop( testSize );

   // write global metadata into a separate file
   std::map< std::string, std::string > metadata = TNL::Benchmarks::getHardwareMetadata();
   TNL::Benchmarks::writeMapAsJson( metadata, log_file_name, ".metadata.json" );

   for( int i = 0; i < testSize; i++ )
      sizes[ i ] = ( double ) std::rand() / ( double ) RAND_MAX * testSize;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "allocation", "global array", }
   }));
   auto f_global =  [&] () mutable {
      for( int i = 0; i < testSize; i++ )
         globalArrayTest( i );
      };
#ifdef __APPLE__
   benchmark.time< TNL::Devices::Host >( "host", f_global );
#endif
   benchmark.time< TNL::Devices::Host >( "host", f_global );

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "allocation", "local array", }
   }));
   auto f_local = [&] () mutable {
      for( int i = 0; i < testSize; i++ )
         localArrayTest( i );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_local );

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "allocation", "stack", }
   }));
   auto f_stack = [&] () mutable {
      for( int i = 0; i < testSize; i++ )
         stackTest( sizes[ i ] );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_stack );

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "allocation", "heap", }
   }));
   auto f_heap = [&] () mutable {
      for( int i = 0; i < testSize; i++ )
         heapTest( sizes[ i ] );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_heap );
   std::cout << ctr << std::endl;
}
