/***************************************************************************
                          operations-test.cpp  -  description
                             -------------------
    begin                : Mar 1, 2015
    copyright            : (C) 2015 by Tomas Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <math.h>
#include <stdlib.h>
#include <TNL/Benchmarks/Benchmarks.h>
#include <TNL/Config/ConfigDescription.h>
#include <TNL/Config/parseCommandLine.h>

using namespace std;

template< typename NumType >
void computeSum( const NumType& n1,
                 const NumType& n2,
                 const NumType& n3,
                 const NumType& n4,
                 const NumType& n5,
                 const NumType& n6,
                 const NumType& n7,
                 const NumType& n8,
                 NumType& sum)
{
   sum += n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8;
}

template< typename NumType >
void computeSumFast( const NumType& n1,
                     const NumType& n2,
                     const NumType& n3,
                     const NumType& n4,
                     const NumType& n5,
                     const NumType& n6,
                     const NumType& n7,
                     const NumType& n8,
                     NumType& sum)
{
   sum += ( n1 + n2 ) + ( n3 + n4 ) + ( n5 + n6 ) + ( n7 + n8 );
}

template< typename NumType >
void computeSumFast2( const NumType& n1,
                      const NumType& n2,
                      const NumType& n3,
                      const NumType& n4,
                      const NumType& n5,
                      const NumType& n6,
                      const NumType& n7,
                      const NumType& n8,
                      NumType& sum)
{
   sum += ( ( n1 + n2 ) + ( n3 + n4 ) ) + ( ( n5 + n6 ) + ( n7 + n8 ) );
}

void configSetup( TNL::Config::ConfigDescription& config )
{
   config.addDelimiter("Benchmark settings:");
   config.addEntry<TNL::String>("log-file", "Log file name.", "operations.log");
   config.addEntry<TNL::String>("output-mode", "Mode for opening the log file.", "overwrite");
   config.addEntryEnum("overwrite");
   config.addEntryEnum("append");
   config.addEntry< bool >( "verbose", "Verbose mode.", true );
}

int main( int argc, char* argv[] )
{
   const int size = 65536;
   const unsigned  long long int loops = 2048;

   TNL::Config::ConfigDescription config;
   configSetup( config );
   TNL::Config::ParameterContainer parameters;
   if( !TNL::Config::parseCommandLine( argc, argv, config, parameters ) )
      return EXIT_FAILURE;

   std::ofstream dummy_file( "dummy.log", ios::out );
   TNL::Benchmarks::Benchmark<> dummy_benchmark( dummy_file, 1, 0 );
   auto f_dummy = [&] () { return;};
   dummy_benchmark.time< TNL::Devices::Host >( "host", f_dummy );

   const auto verbose = parameters.getParameter< bool >( "verbose" );
   const auto log_file_name = parameters.getParameter< TNL::String >( "log-file" );
   const auto output_mode = parameters.getParameter< TNL::String >( "output-mode" );
   auto mode = std::ios::out;
   if( output_mode == "append" )
      mode |= std::ios::app;
   std::ofstream log_file( log_file_name.getString(), mode );
   TNL::Benchmarks::Benchmark<> benchmark(log_file, loops, verbose);
   benchmark.setOperationsPerLoop( size );

   // write global metadata into a separate file
   std::map< std::string, std::string > metadata = TNL::Benchmarks::getHardwareMetadata();
   TNL::Benchmarks::writeMapAsJson( metadata, log_file_name, ".metadata.json" );

   int* integerData = new int[ size ];
   float* floatData = new float[ size ];
   double* doubleData = new double[ size ];

   for( int i = 0; i < size; i++ )
      integerData[ i ] = floatData[ i ] = doubleData[ i ] = 9;

   int integerResult( 0 );
   float floatResult( 0.0 );
   double doubleResult( 0.0 );

   ////
   // sumations
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "integer sum" }
   }));
   integerResult = 0;
   auto f_integer_sum = [&] () mutable {
      for( int i = 0; i < size - 8; i++ )
         computeSum( integerData[ i ],
                     integerData[ i + 1 ],
                     integerData[ i + 2 ],
                     integerData[ i + 3 ],
                     integerData[ i + 4 ],
                     integerData[ i + 5 ],
                     integerData[ i + 6 ],
                     integerData[ i + 7 ],
                     integerResult ); };
#ifdef __APPLE__
   benchmark.time< TNL::Devices::Host >( "host", f_integer_sum );
#endif
   benchmark.time< TNL::Devices::Host >( "host", f_integer_sum );
   std::cout << "Result = " << integerResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "fast integer sum" }
   }));
   integerResult = 0;
   auto f_integer_fast_sum =  [&] () mutable {
      for( int i = 0; i < size - 8; i++ )
         computeSumFast( integerData[ i ],
                         integerData[ i + 1 ],
                         integerData[ i + 2 ],
                         integerData[ i + 3 ],
                         integerData[ i + 4 ],
                         integerData[ i + 5 ],
                         integerData[ i + 6 ],
                         integerData[ i + 7 ],
                         integerResult ); };
   benchmark.time< TNL::Devices::Host >( "host", f_integer_fast_sum );
   std::cout << "Result = " << integerResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "fast integer sum 2" }
   }));
   integerResult = 0;
   auto f_integer_fast_sum_2 =  [&] () mutable {
      for( int i = 0; i < size - 8; i++ )
         computeSumFast2( integerData[ i ],
                         integerData[ i + 1 ],
                         integerData[ i + 2 ],
                         integerData[ i + 3 ],
                         integerData[ i + 4 ],
                         integerData[ i + 5 ],
                         integerData[ i + 6 ],
                         integerData[ i + 7 ],
                         integerResult ); };
   benchmark.time< TNL::Devices::Host >( "host", f_integer_fast_sum_2 );
   std::cout << "Result = " << integerResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "float sum" }
   }));
   floatResult = 0;
   auto f_float_sum = [&] () mutable {
      for( int i = 0; i < size - 8; i++ )
         computeSum( floatData[ i ],
                     floatData[ i + 1 ],
                     floatData[ i + 2 ],
                     floatData[ i + 3 ],
                     floatData[ i + 4 ],
                     floatData[ i + 5 ],
                     floatData[ i + 6 ],
                     floatData[ i + 7 ],
                     floatResult ); };
   benchmark.time< TNL::Devices::Host >( "host", f_float_sum  );
   std::cout << "Result = " << floatResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "fast float sum" }
   }));
   floatResult = 0;
   auto f_fast_float_sum = [&] () mutable {
      for( int i = 0; i < size - 8; i++ )
         computeSumFast( floatData[ i ],
                         floatData[ i + 1 ],
                         floatData[ i + 2 ],
                         floatData[ i + 3 ],
                         floatData[ i + 4 ],
                         floatData[ i + 5 ],
                         floatData[ i + 6 ],
                         floatData[ i + 7 ],
                         floatResult ); };
   benchmark.time< TNL::Devices::Host >( "host", f_fast_float_sum );
   std::cout << "Result = " << floatResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "fast float sum 2" }
   }));
   floatResult = 0;
   auto f_fast_float_sum_2 = [&] () mutable {
      for( int i = 0; i < size - 8; i++ )
         computeSumFast2( floatData[ i ],
                          floatData[ i + 1 ],
                          floatData[ i + 2 ],
                          floatData[ i + 3 ],
                          floatData[ i + 4 ],
                          floatData[ i + 5 ],
                          floatData[ i + 6 ],
                          floatData[ i + 7 ],
                          floatResult ); };
   benchmark.time< TNL::Devices::Host >( "host", f_fast_float_sum_2 );
   std::cout << "Result = " << floatResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "double sum" }
   }));
   doubleResult = 0;
   auto f_double_sum = [&] () mutable {
      for( int i = 0; i < size - 8; i++ )
         computeSum( doubleData[ i ],
                     doubleData[ i + 1 ],
                     doubleData[ i + 2 ],
                     doubleData[ i + 3 ],
                     doubleData[ i + 4 ],
                     doubleData[ i + 5 ],
                     doubleData[ i + 6 ],
                     doubleData[ i + 7 ],
                     doubleResult ); };
   benchmark.time< TNL::Devices::Host >( "host", f_double_sum );
   std::cout << "Result = " << doubleResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "fast double sum" }
   }));
   doubleResult = 0;
   auto f_fast_double_sum = [&] () mutable {
      for( int i = 0; i < size - 8; i++ )
         computeSumFast( doubleData[ i ],
                         doubleData[ i + 1 ],
                         doubleData[ i + 2 ],
                         doubleData[ i + 3 ],
                         doubleData[ i + 4 ],
                         doubleData[ i + 5 ],
                         doubleData[ i + 6 ],
                         doubleData[ i + 7 ],
                         doubleResult ); };
   benchmark.time< TNL::Devices::Host >( "host", f_fast_double_sum );
   std::cout << "Result = " << doubleResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "fast double sum 2" }
   }));
   doubleResult = 0;
   auto f_fast_double_sum_2 = [&] () mutable {
      for( int i = 0; i < size - 8; i++ )
         computeSumFast2( doubleData[ i ],
                          doubleData[ i + 1 ],
                          doubleData[ i + 2 ],
                          doubleData[ i + 3 ],
                          doubleData[ i + 4 ],
                          doubleData[ i + 5 ],
                          doubleData[ i + 6 ],
                          doubleData[ i + 7 ],
                          doubleResult ); };
   benchmark.time< TNL::Devices::Host >( "host", f_fast_double_sum_2 );
   std::cout << "Result = " << doubleResult <<  std::endl;

   ////
   // modulo
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "int % 3" }
   }));
   integerResult = 0;
   auto f_modulo_3 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         integerResult += integerData[ i ] % 3;
      };
   benchmark.time< TNL::Devices::Host >( "host", f_modulo_3 );
   std::cout << "Result = " << integerResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "int % 2" }
   }));
   integerResult = 0;
   auto f_modulo_2 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         integerResult += integerData[ i ] % 2;
      };
   benchmark.time< TNL::Devices::Host >( "host", f_modulo_2 );
   std::cout << "Result = " << integerResult <<  std::endl;

   ////
   // division
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "int / 3" }
   }));
   integerResult = 0;
   auto f_int_div_3 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         integerResult += integerData[ i ] / 3;
      };
   benchmark.time< TNL::Devices::Host >( "host", f_int_div_3 );
   std::cout << "Result = " << integerResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "int / 2" }
   }));
   integerResult = 0;
   auto f_int_div_2 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         integerResult += integerData[ i ] / 2;
      };
   benchmark.time< TNL::Devices::Host >( "host", f_int_div_2 );
   std::cout << "Result = " << integerResult <<  std::endl;

   ////
   // multiplication
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "int * 3" }
   }));
   integerResult = 0;
   auto f_int_mult_3 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         integerResult += integerData[ i ] * 3;
      };
   benchmark.time< TNL::Devices::Host >( "host", f_int_mult_3 );
   std::cout << "Result = " << integerResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "int * 2" }
   }));
   integerResult = 0;
   auto f_int_mult_2 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         integerResult += integerData[ i ] * 2;
      };
   benchmark.time< TNL::Devices::Host >( "host", f_int_mult_2 );
   std::cout << "Result = " << integerResult <<  std::endl;

   ////
   // division by 3
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "float / 3" }
   }));
   floatResult = 0;
   auto f_float_div_3 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         floatResult += floatData[ i ] / 3;
      };
   benchmark.time< TNL::Devices::Host >( "host", f_float_div_3 );
   std::cout << "Result = " << floatResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "double / 3" }
   }));
   floatResult = 0;
   auto f_double_div_3 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         doubleResult += doubleData[ i ] / 3;
      };
   benchmark.time< TNL::Devices::Host >( "host", f_double_div_3 );
   std::cout << "Result = " << doubleResult <<  std::endl;

   ////
   // multiplication by 1.0 / 3.0
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "float * (1.0/3.0)" }
   }));
   floatResult = 0;
   auto f_float_mult = [&] () mutable {
      for( int i = 0; i < size; i++ )
         floatResult += floatData[ i ] * ( float ) ( 1.0 / 3.0 );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_float_mult );
   std::cout << "Result = " << floatResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "double * (1.0/3.0)" }
   }));
   doubleResult = 0;
   auto f_double_mult = [&] () mutable {
      for( int i = 0; i < size; i++ )
         doubleResult += doubleData[ i ] * ( double ) ( 1.0 / 3.0 );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_double_mult );
   std::cout << "Result = " << doubleResult <<  std::endl;

   ////
   // pow 2
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "pow( float, 2.0 )" }
   }));
   floatResult = 0;
   auto f_float_pow_2 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         floatResult += ::pow( floatData[ i ], ( float ) 2.0 );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_float_pow_2 );
   std::cout << "Result = " << floatResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "pow( double, 2.0 )" }
   }));
   doubleResult = 0;
   auto f_double_pow_2 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         doubleResult += ::pow( doubleData[ i ], 2.0 );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_double_pow_2 );
   std::cout << "Result = " << doubleResult <<  std::endl;

   ////
   // pow 2.13
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "pow( float, 2.13)" }
   }));
   floatResult = 0;
   auto f_float_pow_213 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         floatResult += ::pow( floatData[ i ], ( float ) 2.13 );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_float_pow_213 );
   std::cout << "Result = " << floatResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "pow( double, 2.13 )" }
   }));
   doubleResult = 0;
   auto f_double_pow_213 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         doubleResult += ::pow( doubleData[ i ], ( double ) 2.13 );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_double_pow_213 );
   std::cout << "Result = " << doubleResult <<  std::endl;

   ////
   // sqrt
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "sqrt(float)" }
   }));
   floatResult = 0;
   auto f_float_sqrt = [&] () mutable {
      for( int i = 0; i < size; i++ )
         floatResult += sqrt( floatData[ i ] );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_float_sqrt );
   std::cout << "Result = " << floatResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "sqrt( double )" }
   }));
   doubleResult = 0;
   auto f_double_sqrt = [&] () mutable {
      for( int i = 0; i < size; i++ )
         doubleResult += sqrt( doubleData[ i ] );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_double_sqrt );
   std::cout << "Result = " << doubleResult <<  std::endl;

   ////
   // exp
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "exp( float )" }
   }));
   floatResult = 0;
   auto f_float_exp = [&] () mutable {
      for( int i = 0; i < size; i++ )
         floatResult += exp( floatData[ i ] );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_float_exp );
   std::cout << "Result = " << floatResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "exp( double )" }
   }));
   doubleResult = 0;
   auto f_double_exp = [&] () mutable {
      for( int i = 0; i < size; i++ )
         doubleResult += exp( doubleData[ i ] );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_double_exp );
   std::cout << "Result = " << doubleResult <<  std::endl;

   ////
   // log10
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "log10( float )" }
   }));
   floatResult = 0;
   auto f_float_log10 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         floatResult += log10( floatData[ i ] );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_float_log10 );
   std::cout << "Result = " << floatResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "log10( double )" }
   }));
   doubleResult = 0;
   auto f_double_log10 = [&] () mutable {
      for( int i = 0; i < size; i++ )
         doubleResult += log10( doubleData[ i ] );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_double_log10 );
   std::cout << "Result = " << doubleResult <<  std::endl;

   ////
   // sin
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "sin( float )" }
   }));
   floatResult = 0;
   auto f_float_sin = [&] () mutable {
      for( int i = 0; i < size; i++ )
         floatResult += sin( floatData[ i ] );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_float_sin );
   std::cout << "Result = " << floatResult <<  std::endl;

   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "sin( double )" }
   }));
   doubleResult = 0;
   auto f_double_sin = [&] () mutable {
      for( int i = 0; i < size; i++ )
         doubleResult += sin( doubleData[ i ] );
      };
   benchmark.time< TNL::Devices::Host >( "host", f_double_sin );
   std::cout << "Result = " << doubleResult <<  std::endl;

   ////
   // int -> float
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "int to float" }
   }));
   auto f_int_to_float = [&] () mutable {
      for( int i = 0; i < size; i++ )
         floatData[ i ] = integerData[ i ];
      };
   benchmark.time< TNL::Devices::Host >( "host", f_int_to_float );
   std::cout << "Result = " << floatData[ size / 2 ] <<  std::endl;

   ////
   // int -> double
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "int to double" }
   }));
   auto f_int_to_double = [&] () mutable {
      for( int i = 0; i < size; i++ )
         doubleData[ i ] = integerData[ i ];
      };
   benchmark.time< TNL::Devices::Host >( "host", f_int_to_double );
   std::cout << "Result = " << doubleData[ size / 2 ] <<  std::endl;

   ////
   // float -> double
   benchmark.setMetadataColumns( TNL::Benchmarks::Benchmark<>::MetadataColumns( {
      { "operation", "float to double" }
   }));
   auto f_float_to_double = [&] () mutable {
      for( int i = 0; i < size; i++ )
         doubleData[ i ] = floatData[ i ];
      };
   benchmark.time< TNL::Devices::Host >( "host", f_float_to_double );
   std::cout << "Result = " << doubleData[ size / 2 ] <<  std::endl;

   delete[] integerData;
   delete[] floatData;
   delete[] doubleData;
}
