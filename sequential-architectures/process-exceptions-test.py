#!/usr/bin/python3

import os
import json
import pandas as pd
from pandas.io.json import json_normalize
import matplotlib.pyplot as plt
import numpy as np
import math
from os.path import exists

sizes = []

####
# Create multiindex for columns
def get_multiindex():
    level1 =  [ 'size', 'time', 'bandwidth', 'CPU cycles' ]
    df_data = [[ '',    '',     '',          '' ]]

    multiColumns = pd.MultiIndex.from_arrays([ level1 ] )
    return multiColumns, df_data

def processDf( df ):
   multicolumns, df_data = get_multiindex()

   frames = []
   out_idx = 0

   for size in sizes:
      aux_df=df.loc[ ( df['size'] == float( size ) ) ]
      new_df = pd.DataFrame( df_data, columns = multicolumns, index = [out_idx] )
      new_df.iloc[0][ ('size') ] = int( size )
      for index, row in aux_df.iterrows():
         time = row[ 'time' ]
         bandwidth = row[ 'bandwidth' ]
         cpu_cycles = row[ 'cycles/op.' ]
         new_df.iloc[0][( 'time') ] = float( time )
         new_df.iloc[0][( 'bandwidth') ] = float( bandwidth )
         new_df.iloc[0][( 'CPU cycles') ] = float( cpu_cycles )
      frames.append( new_df)
      out_idx = out_idx+1
   result = pd.concat( frames )
   return result

#####
# Parse input files
parsed_lines = []
filename = f"exceptions.log"
if not exists( filename ):
    print( f"Skipping non-existing input file {filename} ...." )
print( f"Parsing input file {filename} ...." )
with open( filename ) as f:
    lines = f.readlines()
    for line in lines:
        parsed_line = json.loads(line)
        parsed_lines.append( parsed_line )

df = pd.DataFrame(parsed_lines)

keys = ['cycles/op.' ]

for key in keys:
    if key in df.keys():
        df[key] = pd.to_numeric(df[key])

for column in ['performer','time', 'tm.stddev', '(tm.stddev)/time', 'CPU cycles', \
               'cycles.stddev', 'loops', 'bandwidth', '(cycles stddev)/cycles', 'speedup']:
    df.drop( column, inplace=True, axis=1)


df.columns = [ 'Test','Throw','CPU cycles per exception' ]
df.to_html( 'exceptions.html', formatters={ 'CPU cycles per exception' : '{:,.2f}'.format}, index=False )
df.to_latex( 'exceptions.tex', formatters={ 'CPU cycles per exception' : '{:,.2f}'.format}, index=False )
