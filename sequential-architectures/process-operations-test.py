#!/usr/bin/python3

import os
import json
import pandas as pd
from pandas.io.json import json_normalize
import matplotlib.pyplot as plt
import numpy as np
import math
from os.path import exists

sizes = []

#####
# Parse input files
parsed_lines = []
filename = f"operations.log"
if not exists( filename ):
    print( f"Skipping non-existing input file {filename} ...." )
print( f"Parsing input file {filename} ...." )
with open( filename ) as f:
    lines = f.readlines()
    for line in lines:
        parsed_line = json.loads(line)
        parsed_lines.append( parsed_line )

df = pd.DataFrame(parsed_lines)

keys = ['time','cycles/op.' ]

for key in keys:
    if key in df.keys():
        try:
           df[key] = pd.to_numeric(df[key])
        except ValueError:
           df[key] = pd.to_numeric(-1)
df['time'] = df['time'].apply(lambda x: x*1000000)

for column in ['performer', 'tm.stddev', '(tm.stddev)/time', 'CPU cycles', \
               'cycles.stddev', 'loops', 'bandwidth', '(cycles stddev)/cycles', 'speedup']:
    df.drop( column, inplace=True, axis=1)
c = df.columns.tolist()
df[[c[1], c[2]]] = df[[c[2], c[1]]]
df.columns = [ 'Operation', 'CPU cycles per operation', 'Time [microsec.]' ]
df.to_html( 'operations.html', formatters={ 'CPU cycles per operation' : '{:,.2f}'.format, 'Time [microsec.]' : '{:,.2f}'.format}, index=False )
df.to_latex( 'operations.tex', formatters={ 'CPU cycles per operation' : '{:,.2f}'.format, 'Time [microsec.]' : '{:,.2f}'.format}, index=False )
