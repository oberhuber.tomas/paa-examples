/***************************************************************************
                          false-sharing.cpp  -  description
                             -------------------
    begin                : Mar 11, 2015
    copyright            : (C) 2015 by Tomas Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef HAVE_OMP
#include <omp.h>
#endif
#include <iostream>
#include <string.h>
#include <CPUCyclesCounter.h>

using namespace std;

int main( int argc, char* argv[] )
{
#ifndef HAVE_OMP
   return EXIT_FAILURE;
#else
   const int cacheLineSize( 64 );
   const int elementsPerCacheLine = cacheLineSize / sizeof( int );
   const int maxElements = 1024 * elementsPerCacheLine;
   int data[ maxElements ];
   memset( data, 0, maxElements * sizeof( int ) );

   const long long int loops( 1000000000 );

   int maxThreads( 0 );
#pragma omp parallel
   {
#pragma omp master
      {
         maxThreads = omp_get_num_threads();
      }
   }

   CPUCyclesCounter cpuCycles;
   for( int threads = 2; threads <= maxThreads; threads++ )
   {
      cpuCycles.reset();
      cpuCycles.start();
#pragma omp parallel num_threads( threads ), shared( data )
      {
         const int idx = 0; //omp_get_thread_num();
         for( long long int i = 0; i < loops; i++ )
         {
            data[ idx ] += i;
#pragma omp barriere
         }
      }
      cpuCycles.stop();
      double falseSharingCycles = ( double ) cpuCycles.getCycles() / ( double ) loops;

      cpuCycles.reset();
      cpuCycles.start();
#pragma omp parallel num_threads( threads ), shared( data )
      {
         const int idx = omp_get_thread_num() * elementsPerCacheLine;
         for( long long int i = 0; i < loops; i++ )
         {
            data[ idx ] += i;
#pragma omp barriere
         }
      }
      cpuCycles.stop();
      double noFalseSharingCycles = ( double ) cpuCycles.getCycles() / ( double ) loops;
      std::cout << "Threads = " << threads << ": False sharing => " << falseSharingCycles << " No false sharing => " << noFalseSharingCycles << "   " << data[ 0 ] << std::endl;
   }
#endif
}
