/***************************************************************************
                          numa-mem-node.cpp  -  description
                             -------------------
    begin                : March 14, 2015, 4:10 PM
    copyright            : (C) 2015 by Tomáš Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#ifdef HAVE_NUMA
#include <numaif.h>
#endif
using namespace std;

int main( int argc, char* argv[] )
{
#ifdef HAVE_NUMA 
   const int size = 1 << 24;
   const int elementsPerPage = 4096 / sizeof( int );
   int* data = new int[ size ];
   
   int page( 0 );
   for( int i = 0; i < size; i += elementsPerPage )
   {
      int numaNode;
      
      get_mempolicy( &numaNode, NULL, 0, (void*) &data[ i ], MPOL_F_NODE | MPOL_F_ADDR);
      std::cout << "Page " << page << " is allocated at node " << numaNode << std::endl;
      page ++;
   }
#endif
}