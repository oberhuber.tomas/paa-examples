/***************************************************************************
                          threads-creation.cpp  -  description
                             -------------------
    begin                : Mar 11, 2015
    copyright            : (C) 2015 by Tomas Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef HAVE_OMP
#include <omp.h>
#endif
#include <iostream>
#include <string.h>
#include <CPUCyclesCounter.h>

using namespace std;

int main( int argc, char* argv[] )
{
#ifndef HAVE_OMP
   return EXIT_FAILURE;
#else

   const long long int loops( 1000000 );

   int maxThreads( 0 );
#pragma omp parallel
   {
#pragma omp master
      {
         maxThreads = omp_get_num_threads();
      }
   }


   CPUCyclesCounter cpuCycles;
   for( int threads = 2; threads <= maxThreads; threads++ )
   {
      int data( 0 );
      cpuCycles.reset();
      cpuCycles.start();
      for( int i = 0; i < loops; i++ )
#pragma omp parallel num_threads( threads )
      {
         data++;
      }
      cpuCycles.stop();
      double cycles = ( double ) cpuCycles.getCycles() / ( double ) loops;
      std::cout << "Threads = " << threads << ": Ticks = " << cycles << " " << data << std::endl;
   }
#endif
}
