/***************************************************************************
                          threads-migration.cpp  -  description
                             -------------------
    begin                : Mar 14, 2015
    copyright            : (C) 2015 by Tomas Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef HAVE_OMP
#include <omp.h>
#endif
#include <iostream>
#include <string.h>
#include <CPUCyclesCounter.h>
#include <sched.h>
#include <time.h>

using namespace std;

int main( int argc, char* argv[] )
{
#ifndef HAVE_OMP
   return EXIT_FAILURE;
#else

   std::cout << "Press Ctrl-C to quit." << std::endl;
#pragma omp parallel
   {
      int core( -1 );
      CPUCyclesCounter cpuCycles;
      cpuCycles.reset();      
      cpuCycles.start();
      while( true )
      {
         int currentCore = sched_getcpu();
         if( currentCore != core )
         {
            time_t rawtime;
            struct tm * timeinfo;
            time ( &rawtime );
            timeinfo = localtime ( &rawtime );
            cpuCycles.stop();
            std::cout << "Thread " << omp_get_thread_num() << " is migrating to the core " << currentCore << " after " << cpuCycles.getCycles() << " CPU ticks at " << asctime (timeinfo);
            core = currentCore;
            cpuCycles.reset();
            cpuCycles.start();            
        }
      }
   }
#endif
}

