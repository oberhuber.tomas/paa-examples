/***************************************************************************
                          vectro-addition.cpp  -  description
                             -------------------
    begin                : Mar 4, 2015
    copyright            : (C) 2015 by Tomáš Oberhuber
    email                : tomas.oberhuber@fjfi.cvut.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h> // memalign
#include <CPUCyclesCounter.h>
#include <immintrin.h>

using namespace std;

const int size = 65536;
const unsigned long long int loops = 1024;

typedef float aligned_float __attribute__((__aligned__(16)));

void vectorAddition(const float *v1,
                    const float *v2,
                    const int size,
                    float *sum)
{
   for (int i = 0; i < size; i++)
      sum[i] = v1[i] + v2[i];
}

void alignedVectorAddition(const float *__restrict__ v1,
                           const float *__restrict__ v2,
                           const int size,
                           float *__restrict__ sum)
{
   int i = 0;
   float *_sum = (float *)__builtin_assume_aligned(sum, 16);
   const float *_v1 = (const float *)__builtin_assume_aligned(v1, 16);
   const float *_v2 = (const float *)__builtin_assume_aligned(&v2[i], 16);
   float y(0.0);
   for (i = 0; i < size; i++)
   {
      _sum[i] = _v1[i] + _v2[i];
      /*if( _sum[ i ] < _v1[ i ] )
         _sum[ i ] = _v1[ i ];
      else
         _sum[ i ] = _sum[ i ];*/
      //_sum[ i ] = _sum[ i ] < _v1[ i ] ? _v1[ i ] : _sum[ i ];
      //_sum[i] += ((_sum[i] > _v1[i]) ? _v1[i] : 0 );
      // y += _v1[i];
   }
   //_sum[0] = y;
}

void shiftedVectorAddition(const float *v1,
                           const float *v2,
                           float *sum,
                           const int size)
{
   for (int i = 0; i < size; i++)
      sum[i] = v1[i] + v2[i];
}

float alignedSum(float *v,
                 const int size)
{
   __m128 s1p = _mm_setzero_ps();
   for (int i = 0; i < size; i += 4)
   {
      __m128 v1p = _mm_load_ps(&v[i]);
      s1p = _mm_add_ps(s1p, v1p);
   }
   float res[4] __attribute__((aligned(16)));
   _mm_store_ps(res, s1p);
   // cout << " " << res[ 0 ] << " " << res[ 1 ] << " " << res[ 2 ] << " " << res[ 3 ] << std::endl;
   return (res[0] + res[1]) + (res[2] + res[3]);
}

float alignedSum2(float *v,
                  const int size)
{
   float res[4] __attribute__((aligned(16)));
   memset(res, 0, 4 * sizeof(float));
   float *_v = (float *)__builtin_assume_aligned(v, 16);
   for (int i = 0; i < size; i += 4)
      for (int j = 0; j < 4; j++)
         res[j] += _v[i + j];
   /*{
      res[ 0 ] += _v[ i ];
      res[ 1 ] += _v[ i + 1 ];
      res[ 2 ] += _v[ i + 2 ];
      res[ 3 ] += _v[ i + 3 ];
   }*/
   return (res[0] + res[1]) + (res[2] + res[3]);
}

int main(int argc, char *argv[])
{
   float *v1 = (float *)malloc(::size * sizeof(float));
   float *v2 = (float *)malloc(::size * sizeof(float));
   float *sum = (float *)malloc(::size * sizeof(float));

   float *aligned_v1 = (float *)memalign(16, ::size * sizeof(float));
   float *aligned_v2 = (float *)memalign(16, ::size * sizeof(float));
   float *aligned_sum = (float *)memalign(16, ::size * sizeof(float));

   CPUCyclesCounter cpuCycles;

   for (int i = 0; i < ::size; i++)
   {
      aligned_v1[i] = v1[i] = 1;
      aligned_v2[i] = v2[i] = 2.0 * i;
      aligned_sum[i] = sum[i] = 0.0;
   }

   cpuCycles.reset();
   cpuCycles.start();
   for (unsigned long long int j = 0; j < loops; j++)
      for (int i = 0; i < ::size; i += 16)
         vectorAddition(&v1[i], &v2[i], 16, &sum[i]);
   cpuCycles.stop();
   std::cout << std::setw(30) << "Vector addition : "
             << std::setw(15) << (double)cpuCycles.getCycles() / (double)(loops * ::size)
             << std::setw(25) << " CPU ticks/ element. result = " << sum[0] << std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   for (unsigned long long int j = 0; j < loops; j++)
   {
      for (int i = 0; i < ::size; i += 16)
         alignedVectorAddition(&aligned_v1[i], &aligned_v2[i], 16, &aligned_sum[i]);
   }
   cpuCycles.stop();
   std::cout << std::setw(30) << "Aligned vector addition : "
             << std::setw(15) << (double)cpuCycles.getCycles() / (double)(loops * ::size)
             << std::setw(25) << " CPU ticks/ element. result = " << sum[0] << std::endl;

   // Caching data
   for (unsigned long long int j = 0; j < loops; j++)
   {
      for (int i = 0; i < ::size - 16; i += 16)
         vectorAddition(&v1[i], &v2[i], 16, &sum[i]);
   }

   for (int shift = 0; shift < 16; shift++)
   {
      cpuCycles.reset();
      cpuCycles.start();
      for (unsigned long long int j = 0; j < loops; j++)
      {
         for (int i = 0; i < ::size - 16; i += 16)
            vectorAddition(&v1[i + shift], &v2[i], 16, &sum[i]);
      }
      cpuCycles.stop();
      std::cout << std::setw(27) << "Shifted vector addition " << shift << " : "
                << std::setw(15) << (double)cpuCycles.getCycles() / (double)(loops * (::size - 16))
                << std::setw(25) << " CPU ticks/ element. result = " << sum[0] << std::endl;
   }

   cpuCycles.reset();
   cpuCycles.start();
   for (unsigned long long int j = 0; j < loops; j++)
      sum[0] = alignedSum(aligned_v1, ::size);
   cpuCycles.stop();
   std::cout << std::setw(30) << "Vector sum 1 : "
             << std::setw(15) << (double)cpuCycles.getCycles() / (double)(loops * ::size)
             << std::setw(25) << " CPU ticks/ element. result = " << sum[0] << std::endl;

   cpuCycles.reset();
   cpuCycles.start();
   for (unsigned long long int j = 0; j < loops; j++)
      sum[0] = alignedSum2(aligned_v1, ::size);
   cpuCycles.stop();
   std::cout << std::setw(30) << "Vector sum 2 : "
             << std::setw(15) << (double)cpuCycles.getCycles() / (double)(loops * ::size)
             << std::setw(25) << " CPU ticks/ element. result = " << sum[0] << std::endl;

   free(v1);
   free(v2);
   free(sum);
   free(aligned_v1);
   free(aligned_v2);
   free(aligned_sum);

   return EXIT_SUCCESS;
}
